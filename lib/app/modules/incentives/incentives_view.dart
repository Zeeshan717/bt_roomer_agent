import 'package:bt_roomer_agent/app/modules/onboard_clients/onboard_client_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../utils/step_progress.dart';
import 'incentives_controller.dart';


class IncentivesView extends StatefulWidget {
  const IncentivesView({Key? key}) : super(key: key);

  @override
  State<IncentivesView> createState() => _IncentivesState();
}

class _IncentivesState extends State<IncentivesView> {
  var controller = Get.put(IncentivesController());
  int _selectedIndex =1;
  void _onItemTapped(int index) {
    setState(() {
      if(_selectedIndex != index){
        _selectedIndex = index;
        Get.offAll(OnboardClientView());
      }
    });
  }
  bool isPaymentDone = true;
  bool startCircle = false;
  final List<String> titles = ["Requested\n₹1000", "Approved\n₹1000", "Payment\nReceived"];
  int _curStep = 1;
  @override
  void initState() {
    // TODO: implement initState
    print("----------this is working------------");
    setState((){
      startCircle = true;
    });
    controller.agentIncentiveDetailsApiCall().then((value) => setState((){
      startCircle = false;
    }));
    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
backgroundColor: Colors.white,
      appBar: AppBar(
        toolbarHeight: 80,
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text("Your Incentives",style: TextStyle(color: Colors.black),),
       // leading: Icon(Icons.arrow_back,color: Colors.black,),
        leadingWidth: 16,
        leading: null,
        actions: [
          // CircleAvatar(
          //   radius: 24,
          //   backgroundColor: Color(0xffD9D9D9),),
          // SizedBox(width: 20,)
          InkWell(
            onTap: () {
              showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (BuildContext context) {
                    return SimpleDialog(
                      children: [
                        Column(
                          children: [
                            Text(
                              "Are You sure !",
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Divider(),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [

                                InkWell(
                                  onTap:(){
                                    Get.back();
                                  },
                                  child: Text("Cancel",style: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 18,
                                  ),),
                                ),
                                InkWell(
                                  onTap:(){
                                    // controller.box.erase();
                                    // Get.offAll(LoginView());
                                  },
                                  child: Text("Logout",style: TextStyle(

                                    fontSize: 18,
                                  ),),
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    );
                  });
              // Get.to(IncentivesView());
            },
            child: Padding(
              padding: EdgeInsets.all(16),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Image.network(

                    "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",fit: BoxFit.cover,width: 50,)),
            ),

          ),
          SizedBox(
            width: 20,
          )
        ],
      ),
      body:startCircle?Center(
        child: Container(
          width: 25,
          height: 25,
          child: CircularProgressIndicator(
            color: Colors.black,
          ),
        ),
      ):controller.agentIncentiveList.isEmpty?Container(
        width: double.infinity,
        height: 25,
        child: Text("No matches for the current request"),
      ):
      Container(

       // padding: EdgeInsets.fromLTRB(30, 20, 30, 0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              Card(
                color: Color(0xff13203B),
                margin:EdgeInsets.only(bottom: 0),
                shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(20))),

                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("Total Incentives",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),
                      Text("Rs ${controller.totalIncentive()}",style: TextStyle(color: Colors.white),),
                    ],

                  ),
                ),
              ),
              Container(
                constraints: BoxConstraints(
                    maxHeight:  MediaQuery.of(context).size.height/2
                ),
                padding: EdgeInsets.only(bottom: 20),
                margin:EdgeInsets.only(top: 0),
                decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))),
                //shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))),

                child:SingleChildScrollView(
                  child: Column(
                    children: List.generate(controller.agentIncentiveList.length, (index) {
                      return InkWell(
                        onTap: (){

                          showDialog(
                              barrierColor: Colors.transparent.withOpacity(0.7),
                              context: context,
                              builder: (BuildContext context) => AlertDialog(
                                contentPadding: EdgeInsets.all(0),
                                insetPadding: EdgeInsets.symmetric(
                                    horizontal: 20,
                                    vertical: MediaQuery.of(context).size.height/7.5),
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(20))),


                                content:Container(

                                  child: SingleChildScrollView(
                                    child: Column(
                                      children: [
                                        Card(
                                          color: Color(0xff13203B),
                                          margin:EdgeInsets.only(bottom: 0),
                                          shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(topLeft: Radius.circular(20),topRight: Radius.circular(20))),

                                          child: Padding(
                                            padding: const EdgeInsets.all(20.0),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                Text("Client Details",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),),

                                              ],

                                            ),
                                          ),
                                        ),

                                        Container(

                                          padding: EdgeInsets.only(bottom: 5),
                                          margin:EdgeInsets.only(top: 0),
                                          decoration: BoxDecoration(
                                            // border: Border.all(),
                                              borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))),
                                          // shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))),

                                          child:Column(
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("PG/Hostel Name",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),

                                                    Text("H1",style: TextStyle(fontSize: 16),),

                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("Owner Name",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),

                                                    Text("Abhay",style: TextStyle(fontSize: 16),),


                                                  ],
                                                ),
                                              ),

                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("Address",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),

                                                    Text("Address",style: TextStyle(fontSize: 16),),


                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("Email",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                                                    Text("Abhay@gmail.com",style: TextStyle(fontSize: 16),),


                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("Phone No.",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),

                                                    Text("9564512854",style: TextStyle(fontSize: 16),),


                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("Pan No.",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),

                                                    Text("Pan",style: TextStyle(fontSize: 16),),


                                                  ],
                                                ),
                                              ),
                                              Divider(),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("Total Amount",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),

                                                    Text("1000",style: TextStyle(fontSize: 16),),


                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("Paid",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),

                                                    Text("4000",style: TextStyle(fontSize: 16),),


                                                  ],
                                                ),
                                              ),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("Remaining",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),

                                                    Text("6000",style: TextStyle(fontSize: 16),),


                                                  ],
                                                ),
                                              ),
                                              Divider(),
                                              Padding(
                                                padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                                child: Row(

                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text("Incentive",style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                                                    Text("2000",style: TextStyle(fontSize: 16),),


                                                  ],
                                                ),
                                              ),


                                            ],


                                          ),

                                        ),
                                        //SizedBox(height: 10,)
                                      ],
                                    ),
                                  ),
                                ) ,
                                actions: [
                                  SizedBox(
                                    width:MediaQuery.of(context).size.width,
                                    height: 50,
                                    child: ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                            backgroundColor:  Color(0xff13203B)),
                                        onPressed: (){
                                          Get.back();
                                        }, child: Text("Edit",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)),
                                  ),

                                ],
                              ));
                        },
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                          child: Row(

                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(controller.agentIncentiveList[index].pgName,style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                              Text(controller.agentIncentiveList[index].incentives.toString(),style: TextStyle(fontSize: 16),),
                            ],
                          ),
                        ),
                      );
                    }),


                  ),
                ),

              ),
            ],
          ),
        ),
          SizedBox(
            height: 20,
          ),
          Spacer(),
         /* SizedBox(
            width: MediaQuery.of(context).size.width-40,
            height: 60,
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                    backgroundColor:  Color(0xff13203B)),

                onPressed: (){
                  controller.agentPaymentRequestApiCall(context);
                }, child: Text("Request  For  Payment",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)),
          ),*/
          SizedBox(height: 20,),
          Container(
            height:MediaQuery.of(context).size.height*0.28 ,
            width: double.infinity,
            color: Color(0xffF4F7FF),
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 8,horizontal: 16),
              child: Column(
                children: [
                 Visibility(
                   visible: true
                     ,
                     child: Column(children: [
                   SizedBox(
                     height:16 ,
                   ),
                   Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                     children: [Text("  Allowed  Amt(${controller.incentivePercentList[0].percent}%)"),
                       Text("₹${controller.allowedAmount()} "),],
                   ),
                   SizedBox(
                     height: 16,
                   ),
                   TextFormField(
                      controller: controller.requestedAmtCtx ,
                      onChanged: (value){
                        var allowedAmount = controller.allowedAmount();
                        var currentAmount = double.parse(value.isEmpty?"0.0":value);
                        if(currentAmount>allowedAmount){
                          controller.requestedAmtCtx.text = "";
                        }
                      },
                     cursorColor: Colors.black54,
                     decoration: InputDecoration(

                       isDense: true,
                       prefixIcon: Icon(Icons.currency_rupee,color:Color(0xff000000).withOpacity(.2),),
                       fillColor: Colors.white,
                       filled: true,

                       hintText: "Request Amt.",
                       hintStyle: TextStyle(color: Color(0xff000000).withOpacity(.2)),
                       border:OutlineInputBorder(
                           borderSide: BorderSide(
                               color: Color(0xff000000),
                               width: 2.0
                           ),
                           borderRadius:BorderRadius.circular(10)
                       ),
                       focusedBorder: OutlineInputBorder(
                           borderSide: BorderSide(
                               color: Color(0xff000000).withOpacity(.2),
                               width: 2.0
                           ),
                           borderRadius:BorderRadius.circular(10)
                       ),
                     ),
                   ),
                   SizedBox(
                     height: 16,
                   ),
                   SizedBox(
                     width: MediaQuery.of(context).size.width,
                     height: 60,
                     child: ElevatedButton(
                         style: ElevatedButton.styleFrom(
                             shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                             backgroundColor:  Color(0xff13203B)),

                         onPressed: (){
                           controller.agentPaymentRequestApiCall(context);
                         }, child: Text("Request  For  Payment",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)),
                   ),
                   SizedBox(
                     height: 16,
                   ),
                 ],)),
                  Visibility(
                      visible: false,
                      child: Column(children: [
                        SizedBox(
                          height:16 ,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [Text("Request Detail",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                           ],
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 8.0),
                          child: StepProgressView(width: MediaQuery.of(context).size.width,
                              curStep: 3,
                              color: Color(0xff13203B),
                              titles: titles),
                        ),
                        SizedBox(
                          height: 16,
                        ),
                        SizedBox(
                          width: MediaQuery.of(context).size.width,
                          height: 60,
                          child: ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
                                  backgroundColor:  Color(0xff13203B)),
                              onPressed: (){
                                controller.agentPaymentRequestApiCall(context);
                              }, child: Text("ConFirm",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),)),
                        ),
                      ],)),


                ],
              ),
            ),
          ),


          ],),
      ),
      bottomNavigationBar: BottomNavigationBar(

        currentIndex: _selectedIndex, //New
        onTap: _onItemTapped,
        selectedItemColor: Colors.black,
        items:  <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: SvgPicture.asset("assets/images/Vectorhome.svg"),
              activeIcon: SvgPicture.asset("assets/images/Vectorhomefill.svg") ,
              label: '',
              backgroundColor: Colors.black

          ),
          BottomNavigationBarItem(
              icon: SvgPicture.asset("assets/images/Vectorincentive.svg"),
              activeIcon: SvgPicture.asset("assets/images/Vectorincentivefill.svg") ,
              label: '',
              backgroundColor: Colors.black
          ),

        ],
      ),
    );
  }
}


