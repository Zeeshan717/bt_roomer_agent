

import 'package:bt_roomer_agent/app/models/agent_incentive_model.dart';
import 'package:bt_roomer_agent/app/services/api_service.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../models/login_model.dart';

class IncentivesController extends GetxController{
  GetStorage box = GetStorage();
   List<Datum> agentIncentiveList = [];
   List<IncentivePercentList> incentivePercentList = [];
   TextEditingController requestedAmtCtx = TextEditingController();
  // incentive Details Api
  Future agentIncentiveDetailsApiCall() async{

    String agent_id;
    try{
      var loginResponse = box.read("loginResponse");
      agent_id = loginResponse["id"].toString();
    }catch(e){
      LoginModel loginResponse = box.read("loginResponse");
      agent_id = loginResponse.id.toString();
    }

    await ApiService.agentIncentiveDetailsApi(agent_id).then((value) {
     if(value.success.toString() == "1"){
       print("This is working--------"+value.toString());
       agentIncentiveList = value.data;
       incentivePercentList = value.incentivePercentList;
       print(agentIncentiveList.toString());
     }
    });
}
// request for incentive api
  Future agentPaymentRequestApiCall(BuildContext context) async{
    String agent_id;
    try{
      var loginResponse = box.read("loginResponse");
      agent_id = loginResponse["id"].toString();
    }catch(e){
      LoginModel loginResponse = box.read("loginResponse");
      agent_id = loginResponse.id.toString();
    }
    var request_amount = requestedAmtCtx.text;
    await ApiService.agentPaymentRequestApi(agent_id,request_amount.toString()).then((value) {
      if(value["success"] == "1"){
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          duration: Duration(seconds:1),
          backgroundColor: Colors.transparent,
          elevation: 0,
          content: Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Colors.black,
                border: Border.all(color: Colors.black, width: 3),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x19000000),
                    spreadRadius: 2.0,
                    blurRadius: 8.0,
                    offset: Offset(2, 4),
                  )
                ],
                borderRadius: BorderRadius.circular(4),
              ),
              child: Row(
                //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: 16,
                  ),
                  const Icon(Icons.check, color: Colors.green,),
                  Spacer(),
                  const Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text('Request send', style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                  ),

                  SizedBox(
                    width: 16,
                  ),


                ],
              )
          ),
        ));
      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          content: Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Colors.black,
                border: Border.all(color: Colors.black, width: 3),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x19000000),
                    spreadRadius: 2.0,
                    blurRadius: 8.0,
                    offset: Offset(2, 4),
                  )
                ],
                borderRadius: BorderRadius.circular(4),
              ),
              child: Row(
                //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: 16,
                  ),
                  const Icon(Icons.close, color: Colors.red,),
                  Spacer(),
                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(value.details, style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                  ),

                  SizedBox(
                    width: 16,
                  ),


                ],
              )
          ),
        ));
      }
    });
  }

  double totalIncentive(){
    double totalIncentive = 0.0;
    agentIncentiveList.forEach((element) {
     totalIncentive += element.incentives;
    });
    return totalIncentive;
  }

  double allowedAmount(){
    double allowedAmount = 0.0;
    double totalIncentiveAmount = totalIncentive();
    allowedAmount = (totalIncentiveAmount * incentivePercentList[0].percent)/100;
    return allowedAmount;
  }
}