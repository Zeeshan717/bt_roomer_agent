import 'package:bt_roomer_agent/app/modules/login/login_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../onboard_clients/onboard_client_view.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  var controller = Get.put(LoginController());
  var _formKey = GlobalKey<FormState>();
  bool StartCircularProgressIndicator = false;
  bool _passwordVisible = false;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        // padding: EdgeInsets.fromLTRB(30, 20, 30, 0),

        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          alignment: AlignmentDirectional.bottomCenter,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Positioned(
                top: 10,
                child: Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                  child: SvgPicture.asset(
                    "assets/images/loginScreenImage.svg",
                    width: 250,
                  ),
                )),
            Container(
              height: MediaQuery.of(context).size.height * .65,
              padding: EdgeInsets.fromLTRB(30, 20, 30, 0),
              decoration: BoxDecoration(
                  color: Color(0xffFFFFFF),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey,
                        offset: Offset(0.0, 2.0), //(x,y)
                        blurRadius: 6.0,
                        spreadRadius: 5),
                  ],
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(18),
                      topRight: Radius.circular(18))),
              child: SingleChildScrollView(
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "Login",
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 28,
                                color: Color(0xff13203B)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 35,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Email Address",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                          color: Color(0xffD9D9D9).withOpacity(0.4),
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            width: 1,
                            color: Color(0xffF5F5F5).withOpacity(.20),
                          ),
                        ),
                        child: TextFormField(
                          // validator: (input) =>
                          //     input!.isValidEmail() ? null : "Check your email",
                          controller: controller.emailCtx,
                          style: TextStyle(
                              fontSize: 16, fontWeight: FontWeight.w400),
                          cursorColor: Colors.black,
                          cursorWidth: 1.5,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            hintText: "abc@gmail.com",
                            hintStyle: TextStyle(
                                fontSize: 13.5,
                                letterSpacing: 0.5,
                                color: Color(0xff9A9A9A).withOpacity(0.9)),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            "Password",
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 15),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                          color: Color(0xffD9D9D9).withOpacity(0.4),
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            width: 1,
                            color: Color(0xffF5F5F5).withOpacity(.20),
                          ),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(
                              child: TextFormField(
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'please enter password';
                                  }
                                  return null;
                                },
                                obscureText:!_passwordVisible,
                                controller: controller.passwordCtx,
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w400),
                                cursorColor: Colors.black,
                                cursorWidth: 1.5,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  enabledBorder: InputBorder.none,
                                  focusedBorder: InputBorder.none,
                                  hintText: "password",
                                  hintStyle: TextStyle(
                                      fontSize: 13.5,
                                      letterSpacing: 0.5,
                                      color:
                                          Color(0xff9A9A9A).withOpacity(0.9)),
                                ),
                              ),
                            ),
                            InkWell(
                              onTap: (){
                                _passwordVisible = !_passwordVisible;
                                setState(() {
                                });
                              },
                              child: Icon(
                                _passwordVisible
                                    ? Icons.visibility
                                    : Icons.visibility_off,
                                color: Color(0xff9A9A9A),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 50,
                      ),
                      SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: 60,
                        child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10)),
                                backgroundColor: Color(0xff13203B)),
                            onPressed: () async {
                              if (_formKey.currentState!.validate()) {
                                setState(() {
                                  StartCircularProgressIndicator = true;
                                });
                                await controller.LoginApiCall(context);
                                setState(() {
                                  StartCircularProgressIndicator = false;
                                });
                              }
                              // print("step 1 done");

                              // controller.LoginApiCall();
                              //  Get.to(OnboardClientView());
                            },
                            child: StartCircularProgressIndicator
                                ? Container(
                                    width: 25,
                                    height: 25,
                                    child: CircularProgressIndicator(
                                      color: Colors.white,
                                    ),
                                  )
                                : Text(
                                    "Login",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold),
                                  )),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          InkWell(
                              onTap: (){
                                controller.checkBoxVisible = !controller.checkBoxVisible;
                                setState(() {

                                });
                              },
                              child: Icon(controller.checkBoxVisible?Icons.check_box:Icons.check_box_outline_blank,)),
                          SizedBox(
                            width: 8,
                          ),
                          Text(
                            "Remember Me ?",
                            style: TextStyle(color: Color(0xff13203B)),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Text(
                        "Powered by Zucol Group",
                        style: TextStyle(fontSize: 10,color: Color(0xff13203B).withOpacity(0.5)),
                      ),

                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}
