import 'package:bt_roomer_agent/app/models/login_model.dart';
import 'package:bt_roomer_agent/app/modules/onboard_clients/onboard_client_view.dart';
import 'package:bt_roomer_agent/app/services/api_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get_storage/get_storage.dart';

class LoginController extends GetxController {
  TextEditingController emailCtx = TextEditingController();
  TextEditingController passwordCtx = TextEditingController();
  bool checkBoxVisible = false;

  GetStorage box = GetStorage();



  Future LoginApiCall(BuildContext context) async {
    print("step 2 done");
 //  try{
     await ApiService.loginApi("", emailCtx.text, passwordCtx.text, "1", " ", "",
            " ", " ", " ", " ", " ")
        .then((value){
          LoginModel loginModel = value;
          print("loging controller value"+value.toString());
          if(value.success.toString() == "1"){
            print("Login Data-----"+value.toString());
            box.write("loginResponse", loginModel);
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              duration: Duration(seconds:1),
              backgroundColor: Colors.transparent,
              elevation: 0,
              content: Container(
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: Colors.black,
                    border: Border.all(color: Colors.black, width: 3),
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0x19000000),
                        spreadRadius: 2.0,
                        blurRadius: 8.0,
                        offset: Offset(2, 4),
                      )
                    ],
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Row(
                    //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        width: 16,
                      ),
                      const Icon(Icons.check, color: Colors.green,),
                      Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text('Successfully logged', style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                      ),

                      SizedBox(
                        width: 16,
                      ),


                    ],
                  )
              ),
            ));
            Future.delayed(Duration(microseconds:3000 ),(){
              Get.to(OnboardClientView());
            });
            if(checkBoxVisible){
              box.write('isLoggedIn', true);
            }else{
              box.write('isLoggedIn', false);
            }

        //    Get.to(OnboardClientView());

          }else{
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              content: Container(
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: Colors.black,
                    border: Border.all(color: Colors.black, width: 3),
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0x19000000),
                        spreadRadius: 2.0,
                        blurRadius: 8.0,
                        offset: Offset(2, 4),
                      )
                    ],
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Row(
                    //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        width: 16,
                      ),
                      const Icon(Icons.close, color: Colors.red,),
                      Spacer(),
                       Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text(value.details, style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                      ),

                      SizedBox(
                        width: 16,
                      ),


                    ],
                  )
              ),
            ));
          }
        });
  // }catch(e){
   // print("!!! Exception Fount !!!");
   // print(e.toString());
 //  }
  }
}
