
import 'dart:io';
import 'dart:typed_data';

import 'package:bt_roomer_agent/app/services/api_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:image_picker/image_picker.dart';

import '../../models/login_model.dart';
import '../../utils/utils.dart';
import '../onboard_clients/onboard_client_view.dart';
enum paymentModeEnum { Cash, Cheque, Online }
class AddPaymentController extends GetxController{
  GetStorage box = GetStorage();
  bool isSelectedPayment = false;
  paymentModeEnum? groupValue = paymentModeEnum.Cash;
  String paymentMode = "Cash";
  TextEditingController amountController = TextEditingController();
 // TextEditingController paidAmountController = TextEditingController();
  TextEditingController paymentDateController = TextEditingController();
  TextEditingController transactionIdController = TextEditingController();
  File? image;

  // uploadFromGallery1() async{
  //   Uint8List img =  await pickImage(ImageSource.gallery);
  //   if(img != null){
  //     image = img;
  //     print(image.toString());
  //   }
  // }

  Future uploadFromGallery() async {
    // ignore: deprecated_member_use
    PickedFile? pickedFile = await ImagePicker().getImage(source: ImageSource.gallery, imageQuality: 30);
    if (pickedFile != null) {
      image = File(pickedFile.path);
    }
  }


  Future addPaymentApiCall(String pg_id) async{
    String agent_id;
    try{
      var loginResponse = box.read("loginResponse");
      agent_id = loginResponse["id"].toString();
    }catch(e){
      LoginModel loginResponse = box.read("loginResponse");
      agent_id = loginResponse.id.toString();
    }
    await ApiService.addPaymentApi(pg_id, agent_id, amountController.text, paymentMode, paymentDateController.text,
        "1", transactionIdController.text, image!).then((value) {
          Get.offAll(OnboardClientView());
    });
  }



}