
import 'package:bt_roomer_agent/app/models/agent_onboard_model.dart';
import 'package:bt_roomer_agent/app/modules/payment/add_payment_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/utils.dart';

class AddPaymentView extends StatefulWidget {
  String? pgId;
  Datum? filterPgInfoList;
  AddPaymentView(this.filterPgInfoList, {Key? key,this.pgId}) : super(key: key);

  @override
  State<AddPaymentView> createState() => _AddPaymentViewState();
}

class _AddPaymentViewState extends State<AddPaymentView> {
  var controller= Get.put(AddPaymentController());
 // List<String> clientData = [];
 //  Datum clientData = Get.arguments[0];
  DateTime? now;
  bool StartCircularProgressIndicator = false;

  @override
  Widget build(BuildContext context) {
    return
        Scaffold(
        resizeToAvoidBottomInset: false,

        appBar:
          AppBar(
          toolbarHeight: 80,
          elevation: 0,
          backgroundColor: Colors.white,
          title: Text(
            "Registration",
            style: TextStyle(color: Colors.black),
          ),
          leading: InkWell(
            onTap: () {
              Get.back();
            },
            child: Icon(
              Icons.arrow_back,
              color: Colors.black,
            ),
          ),
          actions: [
            // CircleAvatar(
            //   radius: 24,
            //   backgroundColor: Color(0xffD9D9D9),
            // ),
            // SizedBox(
            //   width: 20,
            // )
            InkWell(
              onTap: () {
                showDialog(
                    barrierDismissible: false,
                    context: context,
                    builder: (BuildContext context) {
                      return SimpleDialog(
                        children: [
                          Column(
                            children: [
                              Text(
                                "Are You sure !",
                                style: TextStyle(
                                  fontWeight: FontWeight.w400,
                                  fontSize: 18,
                                ),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Divider(),
                              SizedBox(
                                height: 8,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: [

                                  InkWell(
                                    onTap:(){
                                      Get.back();
                                    },
                                    child: Text("Cancel",style: TextStyle(
                                      color: Colors.black38,
                                      fontSize: 18,
                                    ),),
                                  ),
                                  InkWell(
                                    onTap:(){
                                      // controller.box.erase();
                                      // Get.offAll(LoginView());
                                    },
                                    child: Text("Logout",style: TextStyle(

                                      fontSize: 18,
                                    ),),
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      );
                    });
                // Get.to(IncentivesView());
              },
              child: Padding(
                padding: EdgeInsets.all(16),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(50),
                    child: Image.network(

                      "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",fit: BoxFit.cover,width: 50,)),
              ),

            ),
            SizedBox(
              width: 20,
            )
          ],
        ),
        body: Container(
          padding:EdgeInsets.symmetric(horizontal: 16),
          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height:16,
            ),
            Text(widget.filterPgInfoList!.pgName,style: TextStyle(
              fontSize: 20,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            ),),
            Text(widget.filterPgInfoList!.name,style: TextStyle(
              fontSize: 12,
              color: Colors.grey,
            )),
            SizedBox(
              height:20,
            ),
            Container(
                padding: EdgeInsets.only(right: 18, left: 4),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child: Column(
                  children: [
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Radio(
                                fillColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states){
                                  return Colors.black;
                                } ),
                                value: paymentModeEnum.Cash,
                                groupValue: controller.groupValue,
                                onChanged:( value) {
                                  controller.paymentMode = "Cash";
                                  setState(() {
                                    controller.groupValue = value;
                                  });
                                }),
                            Text(
                              "Cash",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                                fillColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states){
                                  return Colors.black;
                                } ),
                                value: paymentModeEnum.Cheque,
                                groupValue: controller.groupValue,
                                onChanged: (value) {
                                  setState(() {
                                    controller.paymentMode = "Cheque";
                                    controller.groupValue = value;
                                  });

                                }),
                            Text(
                              "Cheque",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Radio(
                                fillColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states){
                                  return Colors.black;
                                } ),
                                value: paymentModeEnum.Online,
                                groupValue: controller.groupValue,
                                onChanged: (value) {
                                  setState(() {
                                    controller.paymentMode = "Online";
                                    controller.groupValue=value;
                                  });

                                }),
                            Text(
                              "Online",
                              style: TextStyle(
                                  fontSize: 10,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ],
                    ),
                   controller.paymentMode=="Cash"?Column(
                     children: [
                       SizedBox(height: 5),
                       Row(
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text("Total Amount",
                                   style: TextStyle(
                                       fontSize: 12,
                                       fontWeight: FontWeight.bold))),
                           Expanded(
                             child: SizedBox(
                               height: 45,
                               child:  customTextField(textEditingController: controller.amountController,
                                   textInputType: TextInputType.number,hintText: "",isValidationOn:controller.isSelectedPayment,cOnChanged: (value){
                                     setState((){});
                                   } ),
                             ),
                           ),
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(height: 5),
                       Row(
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text("Payment Date",
                                   style: TextStyle(
                                       fontSize: 12,
                                       fontWeight: FontWeight.bold))),
                           Expanded(
                             child: SizedBox(
                                 height: 45, child:Container(
                               padding: EdgeInsets.symmetric(horizontal: 16),
                               decoration: BoxDecoration(
                                 borderRadius: BorderRadius.circular(10),
                                 border: Border.all(
                                   width: 1,
                                   color: Color(0xff000000).withOpacity(.20),
                                 ),
                               ),
                                   child: TextFormField(
                               enabled: true,
                              controller: controller.paymentDateController,
                             //  validator: isValidationOn?((input)=>input==null || input.isEmpty ?"*This field required":null):null,
                               style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
                               cursorColor: Colors.black,
                               cursorWidth: 1.5,
                               decoration: InputDecoration(
                                   border: InputBorder.none,
                                   enabledBorder: InputBorder.none,
                                   focusedBorder: InputBorder.none,
                                   hintText: "Select Date",
                                   hintStyle:  TextStyle(
                                       fontSize: 12, letterSpacing: 0.5, color: Color(0xffcccccc)),
                               ),
                              // keyboardType: textInputType,
                              // onChanged: cOnChanged,
                               onTap: (){
                                   dateSelected(context);
                               },
                             ),
                                 )
                                 ),
                           ),
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(height: 15),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text(
                                 "Remaining",
                                 style: TextStyle(
                                     fontSize: 12,
                                     fontWeight: FontWeight.bold),
                               )),
                           Text('5000'),
                           // Text('${controller.getRemainingAmount()}'),
                           SizedBox(
                             width: 10,
                           )
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(
                         height: 20,
                       ),
                     ],
                   ): controller.paymentMode=="Cheque"?
                   Column(
                     children: [
                       SizedBox(height: 5),
                       Row(
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text("Cheque Amount",
                                   style: TextStyle(
                                       fontSize: 12,
                                       fontWeight: FontWeight.bold))),
                           Expanded(
                             child: SizedBox(
                               height: 45,
                               child:  customTextField(textEditingController: controller.amountController,
                                   textInputType: TextInputType.number,hintText: "",isValidationOn:controller.isSelectedPayment,cOnChanged: (value){
                                     setState((){});
                                   } ),
                             ),
                           ),
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(height: 5),
                       Row(
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text("Cheque No.",
                                   style: TextStyle(
                                       fontSize: 12,
                                       fontWeight: FontWeight.bold))),
                           Expanded(
                             child: SizedBox(
                                 height: 45, child: customTextField(textEditingController: controller.transactionIdController,
                                 textInputType: TextInputType.number,hintText: "",isValidationOn:controller.isSelectedPayment,cOnChanged: (value){
                                   setState((){});
                                 }  )),
                           ),
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(height: 5),
                       Row(
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text("Bank Name",
                                   style: TextStyle(
                                       fontSize: 12,
                                       fontWeight: FontWeight.bold))),
                           Expanded(
                             child: SizedBox(
                                 height: 45, child: customTextField(textEditingController: controller.transactionIdController,
                                 textInputType: TextInputType.number,hintText: "",isValidationOn:controller.isSelectedPayment,cOnChanged: (value){
                                   setState((){});
                                 }  )),
                           ),
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(height: 5),
                       Row(
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text("Payment Date",
                                   style: TextStyle(
                                       fontSize: 12,
                                       fontWeight: FontWeight.bold))),
                           Expanded(
                             child: SizedBox(
                                 height: 45, child: customTextField(textEditingController: controller.paymentDateController,
                                 textInputType: TextInputType.number,hintText: "yyyy-mm-dd",isValidationOn:controller.isSelectedPayment )),
                           ),
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(height: 15),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text(
                                 "Remaining",
                                 style: TextStyle(
                                     fontSize: 12,
                                     fontWeight: FontWeight.bold),
                               )),
                           Text('5000'),
                           // Text('${controller.getRemainingAmount()}'),
                           SizedBox(
                             width: 10,
                           )
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(
                         height: 20,
                       ),
                     ],
                   ):Column(
                     children: [
                       SizedBox(height: 5),
                       Row(
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text("Total Amount",
                                   style: TextStyle(
                                       fontSize: 12,
                                       fontWeight: FontWeight.bold))),
                           Expanded(
                             child: SizedBox(
                               height: 45,
                               child:  customTextField(textEditingController: controller.amountController,
                                   textInputType: TextInputType.number,hintText: "",isValidationOn:controller.isSelectedPayment,cOnChanged: (value){
                                     setState((){});
                                   } ),
                             ),
                           ),
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(height: 5),
                       Row(
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text("Transaction Id",
                                   style: TextStyle(
                                       fontSize: 12,
                                       fontWeight: FontWeight.bold))),
                           Expanded(
                             child: SizedBox(
                               height: 45,
                               child:  customTextField(textEditingController: controller.amountController,
                                   textInputType: TextInputType.number,hintText: "",isValidationOn:controller.isSelectedPayment,cOnChanged: (value){
                                     setState((){});
                                   } ),
                             ),
                           ),
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(height: 5),
                       Row(
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text("Payment Date",
                                   style: TextStyle(
                                       fontSize: 12,
                                       fontWeight: FontWeight.bold))),
                           Expanded(
                             child: SizedBox(
                                 height: 45, child: customTextField(textEditingController: controller.paymentDateController,
                                 textInputType: TextInputType.number,hintText: "yyyy-mm-dd",isValidationOn:controller.isSelectedPayment )),
                           ),
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(height: 15),
                       Row(
                         mainAxisAlignment: MainAxisAlignment.end,
                         children: [
                           SizedBox(
                             width: 12,
                           ),
                           Expanded(
                               child: Text(
                                 "Remaining",
                                 style: TextStyle(
                                     fontSize: 12,
                                     fontWeight: FontWeight.bold),
                               )),
                           Text('5000'),
                           // Text('${controller.getRemainingAmount()}'),
                           SizedBox(
                             width: 10,
                           )
                           //customTextField("PAN Number"),
                         ],
                       ),
                       SizedBox(
                         height: 20,
                       ),
                     ],
                   )
                  ],
                )),
            SizedBox(
              height: 16,
            ),
            InkWell(
              onTap: () {
                controller
                    .uploadFromGallery()
                    .then((value) => setState(() {}));
              },
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  decoration: BoxDecoration(
                    border: Border.all(width: 1, color: Color(0xff979BA3)),
                    borderRadius: BorderRadius.circular(10)
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 14),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(" Tap To Upload Image"),
                        controller.image != null
                            ? Image.file(
                          controller.image!,
                          width: 100.0,
                          height: 100.0,
                          fit: BoxFit.fitHeight,
                        )
                            : Container(
                          child: Icon(Icons.image),
                        )
                      ],
                    ),
                  )),
            ),
            Spacer(),
            SizedBox(
              width: MediaQuery.of(
                  context)
                  .size
                  .width,
              height: 50,
              child:!StartCircularProgressIndicator?
               ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              10)),
                      backgroundColor: Color(
                          0xff13203B)),
                  onPressed:()async{
                    setState(() {
                      StartCircularProgressIndicator = true;
                    });
                   await controller.addPaymentApiCall(widget.pgId.toString());
                    setState(() {
                      StartCircularProgressIndicator = false;

                    });
                   // Get.to(AddPaymentView(pgId: controller.filterPgInfoList[index].id.toString(),));
                  }
                  ,
                  child:
                  Text(
                    "Add Payment",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold),
                  )):ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(
                              10)),
                      backgroundColor: Color(
                          0xff13203B)),
                  onPressed:(){
                    // setState(() {
                    //   StartCircularProgressIndicator = true;
                    // });
                    // controller.addPaymentApiCall(widget.pgId.toString());
                    // setState(() {
                    //   StartCircularProgressIndicator = false;
                    //
                    // });
                    // Get.to(AddPaymentView(pgId: controller.filterPgInfoList[index].id.toString(),));
                  }
                  ,
                  child:Container(
                    child: CircularProgressIndicator(),
                  )
                  ),
            ),
            SizedBox(
              height: 24,
            )

          ],
          ),
        ),
      );
  }
  dateSelected(BuildContext context) async {
    now = await showDatePicker(
        context: context,
        initialDate: DateTime(DateTime.now().year - 1, 4, 1),
        firstDate: DateTime(DateTime.now().year - 1, 4, 1),
        lastDate: DateTime(DateTime.now().year, 3, 31));
    if(now !=null) {
      controller.paymentDateController.text = now!.toString().split(" ")[0];
    }
  }

}

Widget customTextField( {required TextEditingController textEditingController, required TextInputType textInputType,
  required String hintText, required bool isValidationOn, Function(String)? cOnChanged,}) => Container(
  padding: EdgeInsets.symmetric(horizontal: 16),
  decoration: BoxDecoration(
    borderRadius: BorderRadius.circular(10),
    border: Border.all(
      width: 1,
      color: Color(0xff000000).withOpacity(.20),
    ),
  ),
  child:
  TextFormField(
    controller: textEditingController,
    validator: isValidationOn?((input)=>input==null || input.isEmpty ?"*This field required":null):null,
    style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
    cursorColor: Colors.black,
    cursorWidth: 1.5,
    decoration: InputDecoration(
      border: InputBorder.none,
      enabledBorder: InputBorder.none,
      focusedBorder: InputBorder.none,
      hintText: hintText,
      hintStyle:  TextStyle(
          fontSize: 12, letterSpacing: 0.5, color: Color(0xffcccccc)),
    ),
    keyboardType: textInputType,
    onChanged: cOnChanged,
  ),
);
