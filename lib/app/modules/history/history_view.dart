
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../utils/colors.dart';

class HistoryView extends StatefulWidget {
  const HistoryView({Key? key}) : super(key: key);

  @override
  State<HistoryView> createState() => _HistoryViewState();
}

class _HistoryViewState extends State<HistoryView> {
  bool isSelected = false;
  bool isExpended = false;
  List<bool> currentsSelectedIndex =[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar:
      AppBar(
      toolbarHeight: 80,
      elevation: 0,
      backgroundColor: Colors.white,
      title: Text(
        "History",
        style: TextStyle(color: Colors.black),
      ),
      leading: InkWell(
        onTap: () {
          Get.back();
        },
        child: Icon(
          Icons.arrow_back,
          color: Colors.black,
        ),
      ),
      actions: [
        // CircleAvatar(
        //   radius: 24,
        //   backgroundColor: Color(0xffD9D9D9),
        // ),
        // SizedBox(
        //   width: 20,
        // )
        InkWell(
          onTap: () {
            showDialog(
                barrierDismissible: false,
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    children: [
                      Column(
                        children: [
                          Text(
                            "Are You sure !",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 18,
                            ),
                          ),
                          SizedBox(
                            height: 8,
                          ),
                          Divider(),
                          SizedBox(
                            height: 8,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [

                              InkWell(
                                onTap:(){
                                  Get.back();
                                },
                                child: Text("Cancel",style: TextStyle(
                                  color: Colors.black38,
                                  fontSize: 18,
                                ),),
                              ),
                              InkWell(
                                onTap:(){
                                  // controller.box.erase();
                                  // Get.offAll(LoginView());
                                },
                                child: Text("Logout",style: TextStyle(

                                  fontSize: 18,
                                ),),
                              )
                            ],
                          )
                        ],
                      )
                    ],
                  );
                });
            // Get.to(IncentivesView());
          },
          child: Padding(
            padding: EdgeInsets.all(16),
            child: ClipRRect(
                borderRadius: BorderRadius.circular(50),
                child: Image.network(

                  "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",fit: BoxFit.cover,width: 50,)),
          ),

        ),
        SizedBox(
          width: 20,
        )
      ],
    ),
      body: Container(

        padding:EdgeInsets.symmetric(horizontal: 16),
        width: double.infinity,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 16,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  InkWell(
                    onTap: (){
                      setState(() {
                        isSelected =!isSelected;
                      });
                    },
                    child: Container(
                      decoration:BoxDecoration(
                        color:!isSelected?Color(0xff13203B):Color(0xffF9F9F9),
                        border: Border.all(color: Colors.black26),
                        borderRadius: BorderRadius.only(topLeft:Radius.circular(5),bottomLeft: Radius.circular(5) )
                      ) ,
                      height: 50,
                      width: 150,
                      alignment: Alignment.center,
                      child:Text("Transcations",style: TextStyle(color:!isSelected?Colors.white:Colors.black ),) ,
                    ),
                  ),
                  InkWell(
                    onTap: (){
                      setState(() {
                        isSelected =!isSelected;
                      });
                    },
                    child: Container(
                      decoration:BoxDecoration(
                          color:isSelected?Color(0xff13203B):Color(0xffF9F9F9),
                          border: Border.all(color: Colors.black26),
                          borderRadius: BorderRadius.only(topRight:Radius.circular(5),bottomRight: Radius.circular(5) )
                          
                      ) ,
                      height: 50,
                      width: 150,
                      alignment: Alignment.center,
                      child:Text("Audit Trail",style: TextStyle(color:isSelected?Colors.white:Colors.black ),) ,
                    ),
                  ),

                ],
              ),
              SizedBox(
                height: 20,
              ),
              Visibility(
                visible:!isSelected ,
                  child:Column(
                    children: List.generate(9, (index) {
                      return
                        Container(
                        width:MediaQuery.of(context).size.width,
                        // height: 100,
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(

                              children: [
                                SizedBox(height: 4,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                            decoration: BoxDecoration(
                                                color: Color(0xff13203B),
                                                border: Border.all(),
                                                borderRadius: BorderRadius.circular(15)
                                            ),
                                            height: 50,
                                            width: 50,
                                            child: Icon(Icons.call_received,color: Colors.white,)),
                                        SizedBox(width: 8,),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Received from",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                                            Text("Ashok Boys Pg",style: TextStyle(fontSize: 12,color: Colors.black26),),

                                          ],
                                        ),],
                                    ),
                                    Text("₹ 4000",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),

                                  ],
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [

                                    Text("12/02/2022",style: TextStyle(fontSize: 10,color: Colors.black54),),
                                    Text("Cash",style: TextStyle(fontSize: 10,color: Colors.black54),),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
                  )
                  ),
              Visibility(
                  visible:isSelected ,
                  child:Column(
                    children: List.generate(9, (index) {
                      try{
                        currentsSelectedIndex[index];
                      }catch(e){
                        currentsSelectedIndex.add(false);
                      };
                      if(currentsSelectedIndex[index] == true){
                        isExpended = true;
                      }else{
                        isExpended = false;
                      }
                      return Container(
                        width:MediaQuery.of(context).size.width,
                        // height: 100,
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text("Updated on 12/02/022"),
                                    InkWell(
                                      onTap: (){
                                        setState((){
                                          print("--------index------------------"+index.toString());
                                          currentsSelectedIndex[index]= !currentsSelectedIndex[index];
                                        });

                                      },
                                      child: Icon(Icons.expand_circle_down_outlined),
                                    )

                                  ],
                                ),
                                Visibility(
                                  visible:isExpended ,
                                  child: Column(
                                    children: [
                                      Container(
                                        padding: EdgeInsets.only(
                                            bottom:
                                            5),
                                        margin: EdgeInsets
                                            .only(
                                            top: 0),
                                        decoration: BoxDecoration(
                                          // border: Border.all(),
                                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20))),
                                        // shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))),

                                        child:
                                        Column(
                                          children: [

                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(
                                                  20,
                                                  20,
                                                  20,
                                                  0),
                                              child:
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [

                                                  Text(
                                                    "PG/Hostel Name :  ",
                                                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                  ),
                                                  Text("",
                                                 //   controller.filterPgInfoList[index].pgName,
                                                    style: TextStyle(fontSize: 12),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(
                                                  20,
                                                  20,
                                                  20,
                                                  0),
                                              child:
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Owner Name :         ",
                                                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                  ),
                                                   Text("",
                                                   // controller.filterPgInfoList[index].name,
                                                    style: TextStyle(fontSize: 12),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(
                                                  20,
                                                  20,
                                                  20,
                                                  0),
                                              child:
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Address :                   ",
                                                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                  ),

                                                  Flexible(
                                                      child: Text("",
                                                       // controller.filterPgInfoList[index].address,
                                                        style: TextStyle(fontSize: 12),
                                                        textAlign: TextAlign.start,
                                                      )),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(
                                                  20,
                                                  20,
                                                  20,
                                                  0),
                                              child:
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Email :                         ",
                                                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                  ),
                                                  Text("",
                                                   // controller.filterPgInfoList[index].email,
                                                    style: TextStyle(fontSize: 12),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(
                                                  20,
                                                  20,
                                                  20,
                                                  0),
                                              child:
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Phone No :                ",
                                                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                  ),
                                                  Text("",
                                                   // controller.filterPgInfoList[index].contact1,
                                                    style: TextStyle(fontSize: 12),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.fromLTRB(
                                                  20,
                                                  20,
                                                  20,
                                                  0),
                                              child:
                                              Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "Pan No :                     ",
                                                    style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                  ),
                                                  Text("",
                                                 //   controller.filterPgInfoList[index].panNo,
                                                    style: TextStyle(fontSize: 12),
                                                  ),
                                                ],
                                              ),
                                            ),
                                           // Divider(),

                                            /* Divider(),
                                                                              Padding(
                                                                                padding: const EdgeInsets.fromLTRB(
                                                                                    20,
                                                                                    20,
                                                                                    20,
                                                                                    0),
                                                                                child:
                                                                                    Row(
                                                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                  children: [
                                                                                    Text(
                                                                                      "Incentive",
                                                                                      style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                                                                                    ),
                                                                                    isEditButtonOn
                                                                                        ? Container(
                                                                                            width: 150,
                                                                                            child: customTextField(
                                                                                                textEditingController: demo,
                                                                                                textInputType: TextInputType.number,
                                                                                                hintText: "",
                                                                                                isValidationOn: false,
                                                                                                cOnChanged: (value) {
                                                                                                  setState(() {});
                                                                                                }),
                                                                                          )
                                                                                        : Text(
                                                                                            "2000",
                                                                                            style: TextStyle(fontSize: 16),
                                                                                          ),
                                                                                  ],
                                                                                ),
                                                                              ),*/
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                )

                              /*  SizedBox(height: 4,),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Row(
                                      children: [
                                        Container(
                                            decoration: BoxDecoration(
                                                color: Color(0xff13203B),
                                                border: Border.all(),
                                                borderRadius: BorderRadius.circular(15)
                                            ),
                                            height: 50,
                                            width: 50,
                                            child: Icon(Icons.call_received,color: Colors.white,)),
                                        SizedBox(width: 8,),
                                        Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text("Received from",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                                            Text("Ashok Boys Pg",style: TextStyle(fontSize: 12,color: Colors.black26),),

                                          ],
                                        ),],
                                    ),
                                    Text("₹ 4000",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),

                                  ],
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [

                                    Text("12/02/2022",style: TextStyle(fontSize: 10,color: Colors.black54),),
                                    Text("Cash",style: TextStyle(fontSize: 10,color: Colors.black54),),
                                  ],
                                ),*/
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
                  )
              )

            ],
          ),
        ),
      ),
    );;
  }
}
