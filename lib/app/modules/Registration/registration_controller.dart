import 'package:bt_roomer_agent/app/models/login_model.dart';
import 'package:bt_roomer_agent/app/models/state_model.dart';
import 'package:bt_roomer_agent/app/modules/onboard_clients/onboard_client_view.dart';
import 'package:bt_roomer_agent/app/services/api_service.dart';
import 'package:flutter/material.dart' ;

import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import '../../models/city_model.dart';

enum paymentModeEnum { Cash, Cheque, Online }

class RegistrationController extends GetxController {
  GetStorage box = GetStorage();

  List<AllState> stateList = [AllState(id: 1, stateName: 'Andhra Pradesh')];
  List<City> cityList = [City(id: 1, cityName: "Abu Road")];

 

  TextEditingController pg_name = TextEditingController();
  TextEditingController contact_1 = TextEditingController();
  TextEditingController contact_2 = TextEditingController();
  TextEditingController branchType = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController owner_name = TextEditingController();
  TextEditingController owner_mail = TextEditingController();
  TextEditingController pan_no = TextEditingController();
  TextEditingController amount = TextEditingController();
  TextEditingController paidAmount = TextEditingController();
  TextEditingController payment_mode = TextEditingController();
  TextEditingController payment_date = TextEditingController();
  TextEditingController payment_status = TextEditingController();
  TextEditingController transaction_id = TextEditingController();
   

  paymentModeEnum? groupValue = paymentModeEnum.Cash;

  String branchDropdownValue = 'Boys';
  String stateDropdownValue = 'Andhra Pradesh';
  String cityDropdownValue = 'Abu Road';

  String branchId = "1";
  String stateId = "1";
  String cityId = "1";

  bool isSelectedPayment = false;
  String paymentMode = "Cash";

  double remainValue = 0.0;

  Future getStateApiCall() async {
    await ApiService.getStateList().then((value) {
      stateList = value.state;
      //  print(stateList.toString());
    });
  }

  Future getCityApiCall() async {
    await ApiService.getCityList().then((value) {
      cityList = value.city;
    });
  }

  double getRemainingAmount(){
     var totalAmt = double.parse(amount.text.isEmpty?"0.0":amount.text);
     var paidAmt2 = double.parse(paidAmount.text.isEmpty?"0.0":paidAmount.text);
     remainValue = totalAmt - paidAmt2;
     return remainValue;
  }

  Future registrationApiCall(BuildContext context) async {
    String agent_id;
    try{
      var loginResponse = box.read("loginResponse");
      agent_id = loginResponse["id"].toString();
    }catch(e){
      LoginModel loginResponse = box.read("loginResponse");
      agent_id = loginResponse.id.toString();
    }
    print('agent_id:${agent_id}, ');
    print('pg_name:${pg_name.text}, ');
    print('contact_1:${contact_1.text}, ');
    print('contact_2:${contact_2.text}, ');
    print('branchId:${branchId}, ');
    print('address:${address.text}, ');
    print('owner_name:${owner_name.text}, ');
    print('owner_mail:${owner_mail.text}, ');
    print('pan_no:${pan_no.text}, ');
    print('stateId:${stateId}, ');
    print('cityId:${cityId}, ');

    print('isSelectedPayment:${isSelectedPayment.toString()}, ');
    print('payment_mode:${paymentMode},');
    print('amount:${amount.text}, ');
    print('paid_amount:${paidAmount.text}, ');

    print('pg_name:${pg_name.text}, ');
    print('pg_name:${pg_name.text}, ');
    print('pg_name:${pg_name.text}, ');
    print('pg_name:${pg_name.text}, ');

    

    await ApiService.onboardApi(
            pg_name.text,
            agent_id,
            contact_1.text,
            contact_2.text,
            address.text,
            owner_name.text,
            owner_mail.text,
            stateId,
            cityId,
            branchId,
            pan_no.text,
            isSelectedPayment ? "Yes" : "No",
            amount.text,
            paidAmount.text,
            paymentMode,
            payment_date.text,
            remainValue <= 0.0? "paid": (double.parse(paidAmount.text.isEmpty?"0.0":paidAmount.text)) <= 0.0?"partially paid":"pending",
            transaction_id.text)
        .then((value) {
         if(value["success"].toString() == "1"){
             ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              duration: Duration(seconds:1),
              backgroundColor: Colors.transparent,
              elevation: 0,
              content: Container(
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: Colors.black,
                    border: Border.all(color: Colors.black, width: 3),
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0x19000000),
                        spreadRadius: 2.0,
                        blurRadius: 8.0,
                        offset: Offset(2, 4),
                      )
                    ],
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Row(
                    //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        width: 16,
                      ),
                      const Icon(Icons.check, color: Colors.green,),
                      Spacer(),
                      const Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text('Successfully registered', style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                      ),

                      SizedBox(
                        width: 16,
                      ),


                    ],
                  )
              ),
            ));
            Future.delayed(Duration(microseconds:3000 ),(){
              Get.off(OnboardClientView());
            });
         }else{
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
              content: Container(
                
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                    color: Colors.black,
                    border: Border.all(color: Colors.black, width: 3),
                    boxShadow: const [
                      BoxShadow(
                        color: Color(0x19000000),
                        spreadRadius: 2.0,
                        blurRadius: 8.0,
                        offset: Offset(2, 4),
                      )
                    ],
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Wrap(
                   
                    //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      SizedBox(
                        width: 16,
                      ),
//const Icon(Icons.close, color: Colors.red,),
                     
                       Padding(
                        padding: EdgeInsets.only(left: 8.0),
                        child: Text(value["details"].toString(),overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                      ),

                      SizedBox(
                        width: 16,
                      ),


                    ],
                  )
              ),
            ));
          
         }
    });
  }
}
