import 'package:bt_roomer_agent/app/modules/Registration/registration_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';



class RegistrationView extends StatefulWidget {
  const RegistrationView({Key? key}) : super(key: key);

  @override
  State<RegistrationView> createState() => _RegistrationViewState();
}

class _RegistrationViewState extends State<RegistrationView> {
 
  var _formKey = GlobalKey<FormState>();
  var controller = Get.put(RegistrationController());
  bool StartCircularProgressIndicator = false;

  @override
  void initState() {
    // TODO: implement initState
    controller.getStateApiCall().then((value) => setState((){}));
    controller.getCityApiCall().then((value) => setState((){}));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(controller.stateList.map((e) => e.stateName).toList());
    return Scaffold(
      appBar:
      AppBar(
        toolbarHeight: 80,
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          "Registration",
          style: TextStyle(color: Colors.black),
        ),
        leading: InkWell(
          onTap: () {
            Get.back();
          },
          child: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        ),
        actions: [
          // CircleAvatar(
          //   radius: 24,
          //   backgroundColor: Color(0xffD9D9D9),
          // ),
          // SizedBox(
          //   width: 20,
          // )
          InkWell(
            onTap: () {
              showDialog(
                  barrierDismissible: false,
                  context: context,
                  builder: (BuildContext context) {
                    return SimpleDialog(
                      children: [
                        Column(
                          children: [
                            Text(
                              "Are You sure !",
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Divider(),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [

                                InkWell(
                                  onTap:(){
                                    Get.back();
                                  },
                                  child: Text("Cancel",style: TextStyle(
                                    color: Colors.black38,
                                    fontSize: 18,
                                  ),),
                                ),
                                InkWell(
                                  onTap:(){
                                    // controller.box.erase();
                                    // Get.offAll(LoginView());
                                  },
                                  child: Text("Logout",style: TextStyle(

                                    fontSize: 18,
                                  ),),
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    );
                  });
              // Get.to(IncentivesView());
            },
            child: Padding(
              padding: EdgeInsets.all(16),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Image.network(

                    "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",fit: BoxFit.cover,width: 50,)),
            ),

          ),
          SizedBox(
            width: 20,
          )
        ],
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Form(
              key:_formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "PG/Hostel Name",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        color: Color(0xff13203B)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  customTextField(textEditingController: controller.pg_name,
                  textInputType: TextInputType.name,hintText: "PG/Hostel Name",isValidationOn:true ),
                  SizedBox(
                    height: 10,
                  ),
                  //row start+
                  Row(
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Contact No. 1",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            customTextField(textEditingController: controller.contact_1,
                  textInputType: TextInputType.phone,hintText: "Contact No. 1",isValidationOn:true ),
                            
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Contact No. 2",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                              customTextField(textEditingController: controller.contact_2,
                  textInputType: TextInputType.phone,hintText: "Contact No. 2",isValidationOn:true ),
                        
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Branch Type",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        color: Color(0xff13203B)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  //  customTextField("Branch Type"),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.symmetric(horizontal: 16),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      border: Border.all(
                        width: 1,
                        color: Color(0xff000000).withOpacity(.20),
                      ),
                    ),
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                         Expanded(
                          child: DropdownButton<String>(
                        
                            iconSize: 0,
                            value: controller.branchDropdownValue,
                            elevation: 16,
                            style:  TextStyle(color:Colors.black),
                            underline: Container(
                              height: 0,
                              color: Colors.deepPurpleAccent,
                            ),
                            focusColor:Color(0xfffafafa).withOpacity(.30) ,
                            autofocus:false,
                            onChanged: (String? value) {
                              // This is called when the user selects an item.
                              controller.branchId = ([ 'Boys','Girls', 'Coed'].indexOf(value!) + 1).toString();
                              setState(() {
                                controller.branchDropdownValue = value;
                              });
                            },
                            items: [ 'Boys','Girls', 'Coed'].map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                         Icon(Icons.arrow_drop_down_outlined),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Address",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        color: Color(0xff13203B)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                    customTextField(textEditingController: controller.address,
                  textInputType: TextInputType.streetAddress,hintText: "Address",isValidationOn:true ),
                  
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Owner Name",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        color: Color(0xff13203B)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                   customTextField(textEditingController: controller.owner_name,
                  textInputType: TextInputType.name,hintText: "Owner Name",isValidationOn:true ),
                 
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Owner Mail",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        color: Color(0xff13203B)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  customTextField(textEditingController: controller.owner_mail,
                  textInputType: TextInputType.emailAddress,hintText: "Owner Mail",isValidationOn:true ),
             
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "PAN Number",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        color: Color(0xff13203B)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                       customTextField(textEditingController: controller.pan_no,
                  textInputType: TextInputType.text,hintText: "PAN Number",isValidationOn:true ),
                 
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    children: [

                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "State",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12),
                            ),
                            SizedBox(
                              height: 10,
                            ),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 16),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                    width: 1,
                    color: Color(0xff000000).withOpacity(.20),
                  ),
                ),
                child:Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: SingleChildScrollView(
                        physics: NeverScrollableScrollPhysics(),

                        child: DropdownButton<String>(
                          isExpanded: true,
                          iconSize: 0,
                          value: controller.stateDropdownValue,
                          elevation: 16,
                          style:  TextStyle(color:Colors.black),
                          underline: Container(
                            height: 0,
                            color: Colors.deepPurpleAccent,
                          ),
                          focusColor:Color(0xfffafafa).withOpacity(.30) ,
                          autofocus:false,
                          onChanged: (String? value) {
                            // This is called when the user selects an item.
                            controller.stateId = (controller.stateList.map((e) => e.stateName).toList().indexOf(value!) + 1).toString();
                            setState(() {
                              controller.stateDropdownValue = value;
                            });
                          },
                          items: controller.stateList.map((e) => e.stateName).toList().map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                      ),
                    ),
                    Icon(Icons.arrow_drop_down_outlined),
                  ],
                ),
              ),
                          ],
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "City",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 12),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          padding: EdgeInsets.symmetric(horizontal: 16),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              width: 1,
                              color: Color(0xff000000).withOpacity(.20),
                            ),
                          ),
                          child:Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Expanded(
                                child: DropdownButton<String>(
                                  isExpanded: true,
                                  iconSize: 0,
                                  value: controller.cityDropdownValue,
                                  elevation: 16,
                                  style:  TextStyle(color:Colors.black),
                                  underline: Container(
                                    height: 0,
                                    color: Colors.deepPurpleAccent,
                                  ),
                                  focusColor:Color(0xfffafafa).withOpacity(.30) ,
                                  autofocus:false,
                                  onChanged: (String? value) {
                                    // This is called when the user selects an item.
                                    controller.cityId = (controller.cityList.map((e) => e.cityName).toList().indexOf(value!)+1).toString();
                                    setState(() {
                                      controller.cityDropdownValue = value;
                                    });
                                  },
                                  items: controller.cityList.map((e) => e.cityName).toList().map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                                ),
                              ),
                              Icon(Icons.arrow_drop_down_outlined),
                            ],
                          ),
                        ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        color: Color(0xff13203B)),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  controller.isSelectedPayment
                      ? Container(
                          padding: EdgeInsets.only(right: 18, left: 4),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              width: 1,
                              color: Color(0xff000000).withOpacity(.20),
                            ),
                          ),
                          child: Column(
                            children: [
                              SizedBox(height: 10),
                              Row(
                                children: [
                                  Checkbox(
                                      activeColor: Color(0xff13203B),
                                      value: controller.isSelectedPayment,
                                      onChanged: (value) {
                                        controller.isSelectedPayment = value!;
                                        setState(() {});
                                      }),
                                  Text(
                                    "Pay Now",
                                    style: TextStyle(
                                        fontSize: 10,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Row(
                                    children: [
                                      Radio(
                                          fillColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states){
                                            return Colors.black;
                                          } ),
                                          value: paymentModeEnum.Cash,
                                          groupValue: controller.groupValue,
                                          onChanged:( value) {
                                            controller.paymentMode = "Cash";
                                            setState(() {
                                              controller.groupValue = value;
                                            });
                                          }),
                                      Text(
                                        "Cash",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Radio(
                                         fillColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states){
                                            return Colors.black;
                                          } ),
                                          value: paymentModeEnum.Cheque,
                                          groupValue: controller.groupValue,
                                          onChanged: (value) {
                                            setState(() {
                                            controller.paymentMode = "Cheque";
                                            controller.groupValue = value;
                                            });
                                              
                                          }),
                                      Text(
                                        "Cheque",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                  Row(
                                    children: [
                                      Radio(
                                         fillColor: MaterialStateProperty.resolveWith<Color>((Set<MaterialState> states){
                                            return Colors.black;
                                          } ),
                                          value: paymentModeEnum.Online,
                                          groupValue: controller.groupValue,
                                          onChanged: (value) {
                                            setState(() {
                                            controller.paymentMode = "Online";
                                            controller.groupValue=value;
                                            });
                                              
                                          }),
                                      Text(
                                        "Online",
                                        style: TextStyle(
                                            fontSize: 10,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Expanded(
                                      child: Text("Total Amount",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold))),
                                  Expanded(
                                    child: SizedBox(
                                      height: 45,
                                      child:  customTextField(textEditingController: controller.amount,
                  textInputType: TextInputType.number,hintText: "",isValidationOn:controller.isSelectedPayment,cOnChanged: (value){
                    setState((){});
                  } ),
                                    ),
                                  ),
                                  //customTextField("PAN Number"),
                                ],
                              ),
                              SizedBox(height: 5),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Expanded(
                                      child: Text("Paid Amount",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold))),
                                  Expanded(
                                    child: SizedBox(
                                        height: 45, child: customTextField(textEditingController: controller.paidAmount,
                  textInputType: TextInputType.number,hintText: "",isValidationOn:controller.isSelectedPayment,cOnChanged: (value){
                    setState((){});
                  }  )),
                                  ),
                                  //customTextField("PAN Number"),
                                ],
                              ),
                                SizedBox(height: 5),
                              Row(
                                children: [
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Expanded(
                                      child: Text("Payment Date",
                                          style: TextStyle(
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold))),
                                  Expanded(
                                    child: SizedBox(
                                        height: 45, child: customTextField(textEditingController: controller.payment_date,
                  textInputType: TextInputType.number,hintText: "yyyy-mm-dd",isValidationOn:controller.isSelectedPayment )),
                                  ),
                                  //customTextField("PAN Number"),
                                ],
                              ),
                              SizedBox(height: 15),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  SizedBox(
                                    width: 12,
                                  ),
                                  Expanded(
                                      child: Text(
                                    "Remaining",
                                    style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold),
                                  )),
                                  Text('${controller.getRemainingAmount()}'),
                                  SizedBox(
                                    width: 10,
                                  )
                                  //customTextField("PAN Number"),
                                ],
                              ),
                              SizedBox(
                                height: 20,
                              ),
                            ],
                          ))
                      : Container(
                          height: 50,
                          padding: EdgeInsets.symmetric(horizontal: 0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              width: 1,
                              color: Color(0xff000000).withOpacity(.20),
                            ),
                          ),
                          child: Row(
                            children: [
                              Checkbox(
                                  activeColor: Color(0xff13203B),
                                  value: controller.isSelectedPayment,
                                  onChanged: (value) {
                                    controller.isSelectedPayment = value!;
                                    setState(() {});
                                  }),
                              Text(
                                "Pay Now",
                                style: TextStyle(
                                    fontSize: 10, fontWeight: FontWeight.bold),
                              )
                            ],
                          )),
                  SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                          height: 50,
                          child: OutlinedButton(
                              style: OutlinedButton.styleFrom(
                                side: BorderSide(
                                    width: 1.0, color: Color(0xff13203B)),
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              onPressed: () {
                                Get.to(RegistrationView());
                              },
                              child: Text(
                                "Cancel",
                                style: TextStyle(
                                    color: Color(0xff13203B),
                                    fontWeight: FontWeight.bold),
                              )),
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      ),
                      Expanded(
                        child: SizedBox(
                          height: 50,
                          child:
                          !StartCircularProgressIndicator?
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  backgroundColor: Color(0xff13203B)),
                              onPressed: () async{
                                if(_formKey.currentState!.validate()){
                                  setState(() {
                                  StartCircularProgressIndicator = true; 
                                  });
                                  await controller.registrationApiCall(context);
                                }else{
                                  print("some field are empty");
                                }
                                setState(() {
                                  StartCircularProgressIndicator = false;
                                    
                                  });
                              //  Get.to(RegistrationView());
                              },
                              child: Text(
                                "Register",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold),
                              )): ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10)),
                                  backgroundColor: Color(0xff13203B)),
                              onPressed: () async{
                              },
                              child:Container(child: CircularProgressIndicator(
                                color: Colors.white ,
                              ),)),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),

                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget customTextField( {required TextEditingController textEditingController, required TextInputType textInputType,
  required String hintText, required bool isValidationOn, Function(String)? cOnChanged,}) => Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            width: 1,
            color: Color(0xff000000).withOpacity(.20),
          ),
        ),
        child: TextFormField(
          controller: textEditingController,
          validator: isValidationOn?((input)=>input==null || input.isEmpty ?"*This field required":null):null,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
          cursorColor: Colors.black,
          cursorWidth: 1.5,
          decoration: InputDecoration(
            border: InputBorder.none,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            hintText: hintText,
            hintStyle:  TextStyle(
                fontSize: 12, letterSpacing: 0.5, color: Color(0xffcccccc)),
          ),
          keyboardType: textInputType,
          onChanged: cOnChanged,
        ),
      );
}
