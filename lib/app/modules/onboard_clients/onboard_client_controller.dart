import 'package:bt_roomer_agent/app/models/agent_onboard_model.dart';
import 'package:bt_roomer_agent/app/models/login_model.dart';
import 'package:bt_roomer_agent/app/modules/onboard_clients/onboard_client_view.dart';
import 'package:bt_roomer_agent/app/services/api_service.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class OnboardClientController extends GetxController{
  GetStorage box = GetStorage();

  TextEditingController pg_name = TextEditingController();
  TextEditingController contact_1 = TextEditingController();
  TextEditingController branchType = TextEditingController();
  TextEditingController address = TextEditingController();
  TextEditingController owner_name = TextEditingController();
  TextEditingController owner_mail = TextEditingController();
  TextEditingController pan_no = TextEditingController();
  TextEditingController amount = TextEditingController();
  TextEditingController paidAmount = TextEditingController();
  TextEditingController payment_mode = TextEditingController();
  TextEditingController payment_date = TextEditingController();
  TextEditingController payment_status = TextEditingController();
  TextEditingController transaction_id = TextEditingController();

   AgentOnboardModel? agentOnboardData;
  List<Datum> pgInfoList = [];
  List<Datum> filterPgInfoList = [];
  bool StartCircularProgressIndicator = false;
//  List<PgInfo> pgInfoList =[];
//  List<PgInfo> filterPgInfoList = [];

  Future getAgentOnboardListApi() async{
    String agent_id;
    try{
      var loginResponse = box.read("loginResponse");
      agent_id = loginResponse["id"].toString();
    }catch(e){
      LoginModel loginResponse = box.read("loginResponse");
      agent_id = loginResponse.id.toString();
    }
    var loginResponse = box.read("loginResponse");

       
    await ApiService.getAgentOnboardListApi(agent_id).then((value) {
           
          pgInfoList =[];
         if(value.success.toString() == "1" ){
            agentOnboardData = value;
            pgInfoList = value.data;
         }else{

         }
    });
    
  }

  // Edit Api
  Future editClientDetailsApiCall(BuildContext context, String pg_id)async{
    String agent_id;
    try{
      var loginResponse = box.read("loginResponse");
      agent_id = loginResponse["id"].toString();
    }catch(e){
      LoginModel loginResponse = box.read("loginResponse");
      agent_id = loginResponse.id.toString();
    }
    await ApiService.editClientDetailsApi(pg_name.text, owner_name.text, address.text, pan_no.text, agent_id.toString(), pg_id).then((value){
      if(value["success"].toString() == "1"){
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          duration: Duration(seconds:1),
          backgroundColor: Colors.transparent,
          elevation: 0,
          content: Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Colors.black,
                border: Border.all(color: Colors.black, width: 3),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x19000000),
                    spreadRadius: 2.0,
                    blurRadius: 8.0,
                    offset: Offset(2, 4),
                  )
                ],
                borderRadius: BorderRadius.circular(4),
              ),
              child: Row(
                //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: 16,
                  ),
                  const Icon(Icons.check, color: Colors.green,),
                  Spacer(),
                  const Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text('Successfully registered', style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                  ),

                  SizedBox(
                    width: 16,
                  ),


                ],
              )
          ),
        ));

        // Future.delayed(Duration(microseconds:3000 ),(){
        // });

      }else{
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          content: Container(

              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                color: Colors.black,
                border: Border.all(color: Colors.black, width: 3),
                boxShadow: const [
                  BoxShadow(
                    color: Color(0x19000000),
                    spreadRadius: 2.0,
                    blurRadius: 8.0,
                    offset: Offset(2, 4),
                  )
                ],
                borderRadius: BorderRadius.circular(4),
              ),
              child: Wrap(

                //    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  SizedBox(
                    width: 16,
                  ),
//const Icon(Icons.close, color: Colors.red,),

                  Padding(
                    padding: EdgeInsets.only(left: 8.0),
                    child: Text(value["details"].toString(),overflow: TextOverflow.ellipsis, style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold)),
                  ),

                  SizedBox(
                    width: 16,
                  ),


                ],
              )
          ),
        ));

      }
    });
  }

  int getTotalPaidAmount(int index){
    int value =0;
    for (var value1 in filterPgInfoList[index].amountDetails) {
      value = value+value1.paidAmount;
    }
    return value;
  }



}