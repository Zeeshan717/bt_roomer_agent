import 'package:bt_roomer_agent/app/models/agent_onboard_model.dart';
import 'package:bt_roomer_agent/app/modules/login/login_view.dart';
import 'package:bt_roomer_agent/app/modules/payment/add_payment_view.dart';
import 'package:flutter/material.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';

import '../../models/login_model.dart';
import '../../utils/colors.dart';
import '../Registration/registration_view.dart';
import '../history/history_view.dart';
import '../incentives/incentives_view.dart';
import 'onboard_client_controller.dart';

class OnboardClientView extends StatefulWidget {
  const OnboardClientView({Key? key}) : super(key: key);

  @override
  State<OnboardClientView> createState() => _OnboardClientViewState();
}

class _OnboardClientViewState extends State<OnboardClientView> {
  TextEditingController searchController = TextEditingController();
  int _selectedIndex = 0;

  TextEditingController demo = TextEditingController();
  bool isEditButtonOn = false;
  bool StartCircularProgressIndicator = false;

  var controller = Get.put(OnboardClientController());

  void searchByName() {
    controller.filterPgInfoList = [];
    controller.pgInfoList.forEach((element) {
     // if(element.amountDetails.length != 0){
     //  if(element.amountDetails[element.amountDetails.length-1].paymentStatus.toLowerCase() == "paid"){
     //
     //  }else{
     //
     //  }
     // }
      if (element.pgName
              .toLowerCase()
              .contains(searchController.text.toLowerCase().toString()) ||
          element.name
              .toLowerCase()
              .contains(searchController.text.toLowerCase().toString())
         // ||
        //  element.amountDetails.length!=0?element.amountDetails[element.amountDetails.length-1].paymentStatus.toLowerCase().contains(searchController.text.toLowerCase().toString()):"pending".toLowerCase().contains(searchController.text.toLowerCase().toString())

      ) {
        controller.filterPgInfoList.add(element);
      }
    });
  }

  void _onItemTapped(int index) {
    setState(() {
      if(_selectedIndex != index){
        _selectedIndex = index;
        Get.to(IncentivesView());
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    controller.getAgentOnboardListApi().then((value) {
      setState(() {
        controller.filterPgInfoList = controller.pgInfoList;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var fullName;
    try{
      var loginResponse = controller.box.read("loginResponse");
      fullName   = loginResponse["full_name"].toString();
    }catch(e){
      LoginModel loginResponse = controller.box.read("loginResponse");
      fullName = loginResponse.fullName.toString();
    }


    return Scaffold(
      appBar:
      AppBar(
        toolbarHeight: 80,
        elevation: 0,
        backgroundColor: Colors.white,
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Welcome",
              style: TextStyle(fontSize: 13, color: Colors.black),
            ),
            Text(
              fullName,
              style: TextStyle(color: Colors.black),
            )
          ],
        ),
        leadingWidth: 16,
        leading: IconButton(
          onPressed: () {},
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
        ),
        // leading: IconButton(
        //   onPressed: (() => Get.back()),
        //   icon: Icon(
        //     Icons.arrow_back,
        //     color: Colors.black,
        //   ),
        // ),

        actions: [
          InkWell(
            onTap: () {
              showDialog(
                barrierDismissible: false,
                  context: context,
                  builder: (BuildContext context) {
                    return
                      SimpleDialog(
                      children: [
                        Column(
                          children: [
                            Text(
                              "Are You sure !",
                              style: TextStyle(
                                fontWeight: FontWeight.w400,
                                fontSize: 18,
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Divider(),
                            SizedBox(
                              height: 8,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [

                                InkWell(
                                  onTap:(){
                                    Get.back();
                                  },
                                  child: Text("Cancel",style: TextStyle(
                                    color: Colors.black38,
                    fontSize: 18,
                    ),),
                                ),
                                InkWell(
                                  onTap:(){
                                    controller.box.erase();
                                    Get.offAll(LoginView());
                                  },
                                  child: Text("Logout",style: TextStyle(

                    fontSize: 18,
                    ),),
                                )
                              ],
                            )
                          ],
                        )
                      ],
                    );
                  });
              // Get.to(IncentivesView());
            },
            child: Padding(
              padding: EdgeInsets.all(16),
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Image.network(

                  "https://images.unsplash.com/flagged/photo-1570612861542-284f4c12e75f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1470&q=80",fit: BoxFit.cover,width: 50,)),
            ),

          ),
          SizedBox(
            width: 20,
          )
        ],
      ),
      body: Container(
        padding: EdgeInsets.fromLTRB(30, 20, 30, 0),
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width - 40,
                height: 60,
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10)),
                        backgroundColor: Color(0xff13203B)),
                    onPressed: () {
                      Get.to(RegistrationView());
                    },
                    child: Text(
                      "+ Register New PG/Hostel",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    )),
              ),
              SizedBox(
                height: 30,
              ),
              Text(
                "Onboarded Clients",
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 20,
              ),
              Container(
                decoration: BoxDecoration(
                    border: Border.all(),
                    borderRadius: BorderRadius.all(Radius.circular(20))),
                child: Column(
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                        margin: EdgeInsets.symmetric(horizontal: 8),
                        padding: EdgeInsets.symmetric(horizontal: 16),
                        decoration: BoxDecoration(
                            border:
                                Border.all(width: 1, color: Color(0xff979BA3)),
                            borderRadius: BorderRadius.circular(10)),
                        child: Row(
                          children: [
                            Expanded(
                              child: Container(
                                child: TextField(
                                  controller: searchController,
                                  onChanged: (value) {
                                    searchByName();
                                    setState(() {});
                                  },
                                  // controller: searchController,
                                  style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400),
                                  cursorColor: Colors.black,
                                  cursorWidth: 1.5,
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
                                    enabledBorder: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    hintText: "Search..",
                                    hintStyle: TextStyle(
                                        fontSize: 12, letterSpacing: 0.5),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 8,
                            ),
                            Container(
                                padding: EdgeInsets.all(4),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(6),
                                  // color: Colors.white,
                                ),
                                child: Icon(

                                  Icons.search_sharp,
                                  color:Color(0xff13203B),
                                ))
                          ],
                        )),
                    Container(
                      constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height * .50),
                      // height: MediaQuery.of(context).size.height* .50,
                      child: controller.filterPgInfoList.isEmpty
                          ? Center(
                              child: CircularProgressIndicator(
                                color: Color(0xff13203B),
                              ),
                            )
                          : DataTable2(
                              headingTextStyle: TextStyle(
                                  fontSize: 8,
                                  color: Color(0xff000000).withOpacity(0.3),
                                  fontWeight: FontWeight.w500),
                              horizontalMargin: 10,
                              columnSpacing: 20,
                              minWidth: 200,
                              columns: [
                                DataColumn(
                                    label: Wrap(children: [
                                  Text(
                                    "PG/Hostel Name",
                                    style: TextStyle(fontSize: 12),
                                  )
                                ])),
                                DataColumn(
                                    label: Text(
                                  "Owner Name",
                                  style: TextStyle(fontSize: 12),
                                )),
                                DataColumn(
                                    label: Text(
                                  "Payment Status",
                                  style: TextStyle(fontSize: 12),
                                )),
                              ],
                              rows: List.generate(
                                  controller.filterPgInfoList.length,
                                  (index) => DataRow(
                                          onLongPress: () {
                                            isEditButtonOn = false;
                                            showDialog(
                                              barrierDismissible: false,
                                                barrierColor: Colors.transparent
                                                    .withOpacity(0.7),
                                                context: context,
                                                builder:
                                                    (BuildContext context) =>
                                                        StatefulBuilder(builder:
                                                            (BuildContext
                                                                    context,
                                                                StateSetter
                                                                    setState) {
                                                          return
                                                            AlertDialog(
                                                            contentPadding:
                                                                EdgeInsets.all(
                                                                    0),
                                                            insetPadding: EdgeInsets.symmetric(
                                                                horizontal: 20,
                                                                vertical: MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .height /
                                                                    7.5),
                                                            shape: RoundedRectangleBorder(
                                                                borderRadius: BorderRadius
                                                                    .all(Radius
                                                                        .circular(
                                                                            20))),
                                                            content: Container(
                                                              child:
                                                                  SingleChildScrollView(
                                                                child: Column(
                                                                  children: [
                                                                    Card(
                                                                      color: Color(
                                                                          0xff13203B),
                                                                      margin: EdgeInsets.only(
                                                                          bottom:
                                                                              0),
                                                                      shape: RoundedRectangleBorder(
                                                                          borderRadius: BorderRadius.only(
                                                                              topLeft: Radius.circular(20),
                                                                              topRight: Radius.circular(20))),
                                                                      child:
                                                                          Padding(
                                                                        padding:
                                                                            const EdgeInsets.all(20.0),
                                                                        child:
                                                                            Row(
                                                                          mainAxisAlignment:
                                                                              MainAxisAlignment.spaceBetween,
                                                                          children: [
                                                                            Text(
                                                                              "Client Details",
                                                                              style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold,),
                                                                            ),
                                                                            Row(
                                                                              children: [
                                                                                InkWell(
                                                                                  onTap: (){
                                                                                    Get.to(HistoryView());
                                                                                  },
                                                                                  child: Icon(Icons.history_outlined,color: Colors.white,),
                                                                                ),
                                                                                SizedBox(
                                                                                  width: 16,
                                                                                ),
                                                                                InkWell(
                                                                                  onTap: (){
                                                                                    Get.back();
                                                                                  },
                                                                                  child: Icon(Icons.highlight_remove_sharp,color: Colors.white,),
                                                                                )
                                                                              ],
                                                                            )
                                                                          ],
                                                                        ),
                                                                      ),
                                                                    ),

                                                                    Container(
                                                                      padding: EdgeInsets.only(
                                                                          bottom:
                                                                              5),
                                                                      margin: EdgeInsets
                                                                          .only(
                                                                              top: 0),
                                                                      decoration: BoxDecoration(
                                                                          // border: Border.all(),
                                                                          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20), bottomRight: Radius.circular(20))),
                                                                      // shape:RoundedRectangleBorder(borderRadius: BorderRadius.only(bottomLeft: Radius.circular(20),bottomRight: Radius.circular(20))),

                                                                      child:
                                                                          Column(
                                                                        children: [
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                20,
                                                                                20,
                                                                                20,
                                                                                0),
                                                                            child:
                                                                                Row(
                                                                              mainAxisAlignment: MainAxisAlignment.start,
                                                                              children: [
                                                                                Text(
                                                                                  "PG/Hostel Name :  ",
                                                                                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                                                ),
                                                                                isEditButtonOn
                                                                                    ? Container(
                                                                                       height: 40,
                                                                                        width: 150,
                                                                                        child: customTextField(
                                                                                            textEditingController: controller.pg_name,
                                                                                            textInputType: TextInputType.number,
                                                                                            hintText: "",
                                                                                            isValidationOn: false,
                                                                                            cOnChanged: (value) {
                                                                                              setState(() {});
                                                                                            }),
                                                                                      )
                                                                                    : Text(
                                                                                        controller.filterPgInfoList[index].pgName,
                                                                                        style: TextStyle(fontSize: 12),
                                                                                      ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                20,
                                                                                20,
                                                                                20,
                                                                                0),
                                                                            child:
                                                                                Row(
                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                              children: [
                                                                                Text(
                                                                                  "Owner Name :         ",
                                                                                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                                                ),
                                                                                isEditButtonOn
                                                                                    ? Container(
                                                                                  height: 40,
                                                                                        width: 150,
                                                                                        child: customTextField(
                                                                                            textEditingController: controller.owner_name,
                                                                                            textInputType: TextInputType.number,
                                                                                            hintText: "",
                                                                                            isValidationOn: false,
                                                                                            cOnChanged: (value) {
                                                                                              setState(() {});
                                                                                            }),
                                                                                      )
                                                                                    : Text(
                                                                                        controller.filterPgInfoList[index].name,
                                                                                        style: TextStyle(fontSize: 12),
                                                                                      ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                20,
                                                                                20,
                                                                                20,
                                                                                0),
                                                                            child:
                                                                                Row(
                                                                                    mainAxisAlignment: MainAxisAlignment.start,
                                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                                              children: [
                                                                                Text(
                                                                                  "Address :                   ",
                                                                                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                                                ),
                                                                                isEditButtonOn
                                                                                    ? Container(
                                                                                  height: 40,
                                                                                        width: 150,
                                                                                        child: customTextField(
                                                                                            textEditingController: controller.address,
                                                                                            textInputType: TextInputType.number,
                                                                                            hintText: "",
                                                                                            isValidationOn: false,
                                                                                            cOnChanged: (value) {
                                                                                              setState(() {});
                                                                                            }),
                                                                                      )
                                                                                    : Flexible(
                                                                                        child: Text(
                                                                                        controller.filterPgInfoList[index].address,
                                                                                        style: TextStyle(fontSize: 12),
                                                                                        textAlign: TextAlign.start,
                                                                                      )),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                20,
                                                                                20,
                                                                                20,
                                                                                0),
                                                                            child:
                                                                                Row(
                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                              children: [
                                                                                Text(
                                                                                  "Email :                         ",
                                                                                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                                                ),
                                                                                Text(
                                                                                        controller.filterPgInfoList[index].email,
                                                                                        style: TextStyle(fontSize: 12),
                                                                                      ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                20,
                                                                                20,
                                                                                20,
                                                                                0),
                                                                            child:
                                                                                Row(
                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                              children: [
                                                                                Text(
                                                                                  "Phone No :                ",
                                                                                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                                                ),
                                                                                 Text(
                                                                                        controller.filterPgInfoList[index].contact1,
                                                                                        style: TextStyle(fontSize: 12),
                                                                                      ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                20,
                                                                                20,
                                                                                20,
                                                                                0),
                                                                            child:
                                                                                Row(
                                                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                                              children: [
                                                                                Text(
                                                                                  "Pan No :                     ",
                                                                                  style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                                                ),
                                                                                isEditButtonOn
                                                                                    ? Container(
                                                                                  height: 40,
                                                                                        width: 150,
                                                                                        child: customTextField(
                                                                                            textEditingController: controller.pan_no,
                                                                                            textInputType: TextInputType.number,
                                                                                            hintText: "",
                                                                                            isValidationOn: false,
                                                                                            cOnChanged: (value) {
                                                                                              setState(() {});
                                                                                            }),
                                                                                      )
                                                                                    : Text(
                                                                                        controller.filterPgInfoList[index].panNo,
                                                                                        style: TextStyle(fontSize: 12),
                                                                                      ),
                                                                              ],
                                                                            ),
                                                                          ),
                                                                          Divider(),
                                                                          controller.filterPgInfoList[index].amountDetails.length>0? Visibility(
                                                                              visible:controller.filterPgInfoList[index].amountDetails.length>0,
                                                                              child:Column(
                                                                                children: [
                                                                                  Padding(
                                                                                    padding: const EdgeInsets.fromLTRB(
                                                                                        20,
                                                                                        20,
                                                                                        20,
                                                                                        0),
                                                                                    child:
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      children: [
                                                                                        Text(
                                                                                          "Total Amount :  ",
                                                                                          style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                                                        ),
                                                                                        Text(
                                                                                          controller.filterPgInfoList[index].amountDetails[0].amount.toString(),
                                                                                                 style: TextStyle(fontSize: 16),
                                                                                               ),
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: const EdgeInsets.fromLTRB(
                                                                                        20,
                                                                                        20,
                                                                                        20,
                                                                                        0),
                                                                                    child:
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      children: [
                                                                                        Text(
                                                                                          "Paid :  ",
                                                                                          style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                                                        ),

                                                                                        Text(
                                                                                          controller.getTotalPaidAmount(index).toString(),
                                                                                                style: TextStyle(fontSize: 12),
                                                                                              ),
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                  Padding(
                                                                                    padding: const EdgeInsets.fromLTRB(
                                                                                        20,
                                                                                        20,
                                                                                        20,
                                                                                        0),
                                                                                    child:
                                                                                    Row(
                                                                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                                      children: [
                                                                                        Text(
                                                                                          "Remaining :",
                                                                                          style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold,color: dialogTextColor),
                                                                                        ),

                                                                                        Text(
                                                                                          (controller.filterPgInfoList[index].amountDetails[0].amount - controller.getTotalPaidAmount(index)).toString(),
                                                                                                style: TextStyle(fontSize: 12,color: Colors.red),
                                                                                              ),
                                                                                      ],
                                                                                    ),
                                                                                  ),
                                                                                ],
                                                                              ) ):Container(),

                                                                         /* Divider(),
                                                                          Padding(
                                                                            padding: const EdgeInsets.fromLTRB(
                                                                                20,
                                                                                20,
                                                                                20,
                                                                                0),
                                                                            child:
                                                                                Row(
                                                                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                                              children: [
                                                                                Text(
                                                                                  "Incentive",
                                                                                  style: TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
                                                                                ),
                                                                                isEditButtonOn
                                                                                    ? Container(
                                                                                        width: 150,
                                                                                        child: customTextField(
                                                                                            textEditingController: demo,
                                                                                            textInputType: TextInputType.number,
                                                                                            hintText: "",
                                                                                            isValidationOn: false,
                                                                                            cOnChanged: (value) {
                                                                                              setState(() {});
                                                                                            }),
                                                                                      )
                                                                                    : Text(
                                                                                        "2000",
                                                                                        style: TextStyle(fontSize: 16),
                                                                                      ),
                                                                              ],
                                                                            ),
                                                                          ),*/
                                                                        ],
                                                                      ),
                                                                    ),
                                                                    //SizedBox(height: 10,)
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                            actions: [
                                                              SizedBox(
                                                                height: 10,
                                                              ),
                                                          /*    SizedBox(
                                                                width: MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width,
                                                                height: 50,
                                                                child:isEditButtonOn?
                                                                ElevatedButton(
                                                                    style: ElevatedButton.styleFrom(
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(
                                                                                10)),
                                                                        backgroundColor: Color(
                                                                            0xff13203B)),
                                                                    onPressed:
                                                                        () async {

                                                                          setState(() {
                                                                            StartCircularProgressIndicator = true;
                                                                          });
                                                                          await controller.editClientDetailsApiCall(context,controller.filterPgInfoList[index].id.toString()).then((value) {

                                                                            controller.getAgentOnboardListApi().then((value) {
                                                                              setState(() {
                                                                                controller.filterPgInfoList = controller.pgInfoList;
                                                                                isEditButtonOn =
                                                                                !isEditButtonOn;
                                                                              });
                                                                              Get.back();
                                                                            });

                                                                          });

                                                                          setState(() {
                                                                            StartCircularProgressIndicator = false;
                                                                          });

                                                                        },
                                                                    child:StartCircularProgressIndicator?
                                                                    Container(
                                                                      child: CircularProgressIndicator(
                                                                        color: Colors.white,
                                                                      ),
                                                                    ):
                                                                    Text(
                                                                      "Submit",
                                                                      style: TextStyle(
                                                                          color: Colors.white,
                                                                          fontWeight: FontWeight.bold),
                                                                    )):
                                                                    ElevatedButton(
                                                                        style: ElevatedButton.styleFrom(
                                                                            shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(
                                                                                    10)),
                                                                            backgroundColor: Color(
                                                                                0xff13203B)),
                                                                        onPressed:
                                                                            ()   {
                                                                              controller
                                                                                  .pg_name
                                                                                  .text = controller.filterPgInfoList[index].pgName;
                                                                              controller
                                                                                  .owner_name
                                                                                  .text = controller.filterPgInfoList[index].name;
                                                                              controller
                                                                                  .address
                                                                                  .text = controller.filterPgInfoList[index].address;
                                                                              controller
                                                                                  .owner_mail
                                                                                  .text = controller.filterPgInfoList[index].email;
                                                                              controller
                                                                                  .contact_1
                                                                                  .text = controller.filterPgInfoList[index].contact1;
                                                                              controller
                                                                                  .pan_no
                                                                                  .text = controller.filterPgInfoList[index].panNo;
                                                                              //    controller.amount.text = controller.agentOnboardData!.data[index];
                                                                              //   controller.pg_name.text = controller.agentOnboardData!.data[index].pgName;

                                                                              setState(
                                                                                      () {
                                                                                    isEditButtonOn =
                                                                                    !isEditButtonOn;
                                                                                  });
                                                                              // Get.back();


                                                                          // Get.back();
                                                                        },
                                                                        child:
                                                                            Text(
                                                                          "Edit",
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontWeight: FontWeight.bold),
                                                                        )),
                                                              ),*/
                                                              Row(
                                                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                                                children: [
                                                                  SizedBox(
                                                                    // width: MediaQuery.of(
                                                                    //     context)
                                                                    //     .size
                                                                    //     .width,
                                                                    width: 140,
                                                                    height: 50,
                                                                    child:isEditButtonOn?
                                                                    ElevatedButton(
                                                                        style: ElevatedButton.styleFrom(
                                                                            shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(
                                                                                    10)),
                                                                            backgroundColor: Color(
                                                                                0xff13203B)),
                                                                        onPressed:
                                                                            () async {

                                                                          setState(() {
                                                                            StartCircularProgressIndicator = true;
                                                                          });
                                                                          await controller.editClientDetailsApiCall(context,controller.filterPgInfoList[index].id.toString()).then((value) {

                                                                            controller.getAgentOnboardListApi().then((value) {
                                                                              setState(() {
                                                                                controller.filterPgInfoList = controller.pgInfoList;
                                                                                isEditButtonOn =
                                                                                !isEditButtonOn;
                                                                              });
                                                                              Get.back();
                                                                            });

                                                                          });

                                                                          setState(() {
                                                                            StartCircularProgressIndicator = false;
                                                                          });

                                                                        },
                                                                        child:StartCircularProgressIndicator?
                                                                        Container(
                                                                          child: CircularProgressIndicator(
                                                                            color: Colors.white,
                                                                          ),
                                                                        ):
                                                                        Text(
                                                                          "Submit",
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontWeight: FontWeight.bold),
                                                                        )):
                                                                    ElevatedButton(
                                                                        style: ElevatedButton.styleFrom(
                                                                            shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(
                                                                                    10)),
                                                                            backgroundColor: Color(
                                                                                0xff13203B)),
                                                                        onPressed:
                                                                            ()   {
                                                                          controller
                                                                              .pg_name
                                                                              .text = controller.filterPgInfoList[index].pgName;
                                                                          controller
                                                                              .owner_name
                                                                              .text = controller.filterPgInfoList[index].name;
                                                                          controller
                                                                              .address
                                                                              .text = controller.filterPgInfoList[index].address;
                                                                          controller
                                                                              .owner_mail
                                                                              .text = controller.filterPgInfoList[index].email;
                                                                          controller
                                                                              .contact_1
                                                                              .text = controller.filterPgInfoList[index].contact1;
                                                                          controller
                                                                              .pan_no
                                                                              .text = controller.filterPgInfoList[index].panNo;
                                                                          //    controller.amount.text = controller.agentOnboardData!.data[index];
                                                                          //   controller.pg_name.text = controller.agentOnboardData!.data[index].pgName;

                                                                          setState(
                                                                                  () {
                                                                                isEditButtonOn =
                                                                                !isEditButtonOn;
                                                                              });
                                                                          // Get.back();


                                                                          // Get.back();
                                                                        },
                                                                        child:
                                                                        Text(
                                                                          "Edit",
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontWeight: FontWeight.bold),
                                                                        )),
                                                                  ),
                                                                  SizedBox(
                                                                    // width: MediaQuery.of(
                                                                    //     context)
                                                                    //     .size
                                                                    //     .width,
                                                                    width: 140,
                                                                    height: 50,
                                                                    child:isEditButtonOn?
                                                                    ElevatedButton(
                                                                        style: ElevatedButton.styleFrom(
                                                                            shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(
                                                                                    10)),
                                                                            backgroundColor: Color(
                                                                                0xff13203B)),
                                                                        onPressed:(){
                                                                         // Get.to(AddPaymentView(pgId: controller.filterPgInfoList[index].id.toString(),));
                                                                            setState((){
                                                                              isEditButtonOn =
                                                                              !isEditButtonOn;
                                                                            });
                                                                            // Get.back();
                                                                        }
                                                                        ,
                                                                        child:
                                                                        Text(
                                                                          "Cancel",
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontWeight: FontWeight.bold),
                                                                        )):
                                                                    ElevatedButton(
                                                                        style: ElevatedButton.styleFrom(
                                                                            shape: RoundedRectangleBorder(
                                                                                borderRadius: BorderRadius.circular(
                                                                                    10)),
                                                                            backgroundColor: Color(
                                                                                0xff13203B)),
                                                                        onPressed:(){
                                                                          Get.to(AddPaymentView(controller.filterPgInfoList[index]));
                                                                        }
                                                                        ,
                                                                        child:
                                                                        Text(
                                                                          "Add Payment",
                                                                          style: TextStyle(
                                                                              color: Colors.white,
                                                                              fontWeight: FontWeight.bold),
                                                                        )),
                                                                  )
                                                                ],
                                                              ),


                                                           /*   SizedBox(
                                                                height: 8,
                                                              ),
                                                              isEditButtonOn?Container():
                                                              SizedBox(
                                                                width: MediaQuery.of(
                                                                    context)
                                                                    .size
                                                                    .width,
                                                                height: 50,
                                                                child:
                                                                ElevatedButton(
                                                                    style: ElevatedButton.styleFrom(
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(
                                                                                10)),
                                                                        backgroundColor: Color(
                                                                            0xff13203B)),
                                                                    onPressed:(){
                                                                      Get.to(AddPaymentView(pgId: controller.filterPgInfoList[index].id.toString(),));
                                                                    }
                                                                        ,
                                                                    child:
                                                                    Text(
                                                                      "Add Payment",
                                                                      style: TextStyle(
                                                                          color: Colors.white,
                                                                          fontWeight: FontWeight.bold),
                                                                    )),
                                                              ),
                                                              SizedBox(
                                                                height: 8,
                                                              ),
                                                              isEditButtonOn?Container():
                                                              SizedBox(
                                                                width: MediaQuery.of(
                                                                    context)
                                                                    .size
                                                                    .width,
                                                                height: 50,
                                                                child:
                                                                ElevatedButton(
                                                                    style: ElevatedButton.styleFrom(
                                                                        shape: RoundedRectangleBorder(
                                                                            borderRadius: BorderRadius.circular(
                                                                                10)),
                                                                        backgroundColor: Color(
                                                                            0xff13203B)),
                                                                    onPressed:(){
                                                                      Get.to(AddPaymentView(pgId: controller.filterPgInfoList[index].id.toString(),));
                                                                    }
                                                                    ,
                                                                    child:
                                                                    Text(
                                                                      "History",
                                                                      style: TextStyle(
                                                                          color: Colors.white,
                                                                          fontWeight: FontWeight.bold),
                                                                    )),
                                                              ),*/
                                                              SizedBox(
                                                                height: 10,
                                                              )
                                                            ],
                                                          );
                                                        }));
                                          },
                                          cells: [
                                            DataCell(Text(controller
                                                .filterPgInfoList[index]
                                                .pgName)),
                                            DataCell(Center(
                                              child: Text(controller
                                                  .filterPgInfoList[index]
                                                  .name,textAlign: TextAlign.center,),
                                            )),
                                            DataCell(Center(
                                              child:controller
                                                  .filterPgInfoList[index].amountDetails.length != 0?
                                              Text(

                                                controller
                                                    .filterPgInfoList[index].amountDetails[controller
                                                    .filterPgInfoList[index].amountDetails.length-1].paymentStatus.toUpperCase(),textAlign: TextAlign.center,
                                                style: TextStyle(

                                                    color: controller
                                                        .filterPgInfoList[index].amountDetails[controller
                                                        .filterPgInfoList[index].amountDetails.length-1].paymentStatus.toUpperCase() ==
                                                        'PENDING'
                                                        ? Colors.red
                                                        : controller
                                                        .filterPgInfoList[index].amountDetails[controller
                                                        .filterPgInfoList[index].amountDetails.length-1].paymentStatus.toUpperCase() ==
                                                        'PAID'
                                                        ? Colors.green
                                                        : Color(
                                                        0xffFF7A00)),
                                              ):Text(

                                                "PENDING",textAlign: TextAlign.center,
                                                style: TextStyle(

                                                    color:Colors.red
                                                       ),
                                              )
                                              ,
                                            )),
                                          ]))),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex, //New
        onTap: _onItemTapped,
        selectedItemColor: Colors.black,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: SvgPicture.asset("assets/images/Vectorhome.svg"),
              activeIcon: SvgPicture.asset("assets/images/Vectorhomefill.svg"),
              label: '',
              backgroundColor: Colors.black),
          BottomNavigationBarItem(
              icon: SvgPicture.asset("assets/images/Vectorincentive.svg"),
              activeIcon:
                  SvgPicture.asset("assets/images/Vectorincentivefill.svg"),
              label: '',
              backgroundColor: Colors.black),
        ],
      ),
    );
  }

  Widget customTextField({
    required TextEditingController textEditingController,
    required TextInputType textInputType,
    required String hintText,
    required bool isValidationOn,
    Function(String)? cOnChanged,
  }) =>
      Container(
        padding: EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
          border: Border.all(
            width: 1,
            color: Color(0xff000000).withOpacity(.20),
          ),
        ),
        child: TextFormField(
          controller: textEditingController,
          validator: isValidationOn
              ? ((input) => input == null || input.isEmpty
                  ? "*This field required"
                  : null)
              : null,
          style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400),
          cursorColor: Colors.black,
          cursorWidth: 1.5,
          decoration: InputDecoration(
            border: InputBorder.none,
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            hintText: hintText,
            hintStyle: TextStyle(
                fontSize: 12, letterSpacing: 0.5, color: Color(0xffcccccc)),
          ),
          keyboardType: textInputType,
          onChanged: cOnChanged,
        ),
      );
}

class PgInfo {
  String id;
  String pgName;
  String ownerName;
  String status;
  PgInfo(this.id, this.pgName, this.ownerName, this.status);
}
