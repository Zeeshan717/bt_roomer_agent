// To parse this JSON data, do
//
//     final agentIncentiveDetailsModel = agentIncentiveDetailsModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

AgentIncentiveDetailsModel agentIncentiveDetailsModelFromJson(String str) => AgentIncentiveDetailsModel.fromJson(json.decode(str));

String agentIncentiveDetailsModelToJson(AgentIncentiveDetailsModel data) => json.encode(data.toJson());

class AgentIncentiveDetailsModel {
  AgentIncentiveDetailsModel({
    required this.data,
    required this.incentivePercentList,
    required this.success,
    required this.details,
  });

  List<Datum> data;
  List<IncentivePercentList> incentivePercentList;
  String success;
  String details;

  factory AgentIncentiveDetailsModel.fromJson(Map<String, dynamic> json) => AgentIncentiveDetailsModel(
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
    incentivePercentList: List<IncentivePercentList>.from(json["incentivePercentList"].map((x) => IncentivePercentList.fromJson(x))),
    success: json["success"],
    details: json["details"],
  );

  Map<String, dynamic> toJson() => {
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
    "incentivePercentList": List<dynamic>.from(incentivePercentList.map((x) => x.toJson())),
    "success": success,
    "details": details,
  };
}

class Datum {
  Datum({
    required this.id,
    required this.pgId,
    required this.agentId,
    required this.transactionsId,
    required this.receiveAmount,
    required this.incentives,
    required this.status,
    required this.createdAt,
    required this.updatedAt,
    required this.pgName,
  });

  int id;
  int pgId;
  int agentId;
  int transactionsId;
  int receiveAmount;
  int incentives;
  int status;
  String createdAt;
  String updatedAt;
  String pgName;

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    id: json["id"],
    pgId: json["pg_id"],
    agentId: json["agent_id"],
    transactionsId: json["transactions_id"],
    receiveAmount: json["receive_amount"],
    incentives: json["incentives"],
    status: json["status"],
    createdAt: json["created_at"],
    updatedAt: json["updated_at"],
    pgName: json["pg_name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "pg_id": pgId,
    "agent_id": agentId,
    "transactions_id": transactionsId,
    "receive_amount": receiveAmount,
    "incentives": incentives,
    "status": status,
    "created_at": createdAt,
    "updated_at": updatedAt,
    "pg_name": pgName,
  };
}

class IncentivePercentList {
  IncentivePercentList({
    required this.id,
    required this.pgId,
    required this.agentId,
    required this.percent,
    required this.insertedAt,
    required this.updatedAt,
  });

  int id;
  int pgId;
  int agentId;
  int percent;
  String insertedAt;
  String updatedAt;

  factory IncentivePercentList.fromJson(Map<String, dynamic> json) => IncentivePercentList(
    id: json["id"],
    pgId: json["pg_id"],
    agentId: json["agent_id"],
    percent: json["percent"],
    insertedAt: json["inserted_at"],
    updatedAt: json["updated_at"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "pg_id": pgId,
    "agent_id": agentId,
    "percent": percent,
    "inserted_at": insertedAt,
    "updated_at": updatedAt,
  };
}
