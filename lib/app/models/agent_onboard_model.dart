// To parse this JSON data, do
//
//     final agentOnboardModel = agentOnboardModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

AgentOnboardModel agentOnboardModelFromJson(String str) => AgentOnboardModel.fromJson(json.decode(str));

String agentOnboardModelToJson(AgentOnboardModel data) => json.encode(data.toJson());

class AgentOnboardModel {
    AgentOnboardModel({
        required this.data,
        required this.success,
        required this.details,
    });

    List<Datum> data;
    String success;
    String details;

    factory AgentOnboardModel.fromJson(Map<String, dynamic> json) => AgentOnboardModel(
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        success: json["success"],
        details: json["details"],
    );

    Map<String, dynamic> toJson() => {
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "success": success,
        "details": details,
    };
}

class Datum {
    Datum({
        required this.id,
        required this.name,
        required this.pgName,
        required this.address,
        required this.email,
        required this.contact1,
        required this.panNo,
        required this.amountDetails,
    });

    int id;
    String name;
    String pgName;
    String address;
    String email;
    String contact1;
    String panNo;
    List<AmountDetail> amountDetails;

    factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        pgName: json["pg_name"],
        address: json["address"],
        email: json["email"],
        contact1: json["contact_1"],
        panNo: json["pan_no"],
        amountDetails: List<AmountDetail>.from(json["amountDetails"].map((x) => AmountDetail.fromJson(x))),
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "pg_name": pgName,
        "address": address,
        "email": email,
        "contact_1": contact1,
        "pan_no": panNo,
        "amountDetails": List<dynamic>.from(amountDetails.map((x) => x.toJson())),
    };
}

class AmountDetail {
    AmountDetail({
        required this.pgId,
        required this.amount,
        required this.paidAmount,
        required this.paymentStatus,
    });

    int pgId;
    int amount;
    int paidAmount;
    String paymentStatus;

    factory AmountDetail.fromJson(Map<String, dynamic> json) => AmountDetail(
        pgId: json["pg_id"],
        amount: json["amount"],
        paidAmount: json["paid_amount"],
        paymentStatus: json["payment_status"],
    );

    Map<String, dynamic> toJson() => {
        "pg_id": pgId,
        "amount": amount,
        "paid_amount": paidAmount,
        "payment_status": paymentStatus,
    };
}
