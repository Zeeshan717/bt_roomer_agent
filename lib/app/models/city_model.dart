
import 'dart:convert';

CityModel cityModelFromJson(String str) => CityModel.fromJson(json.decode(str));

String cityModelToJson(CityModel data) => json.encode(data.toJson());

class CityModel {
  CityModel({
    required this.city,
    required this.success,
    required this.details,
  });

  List<City> city;
  String success;
  String details;

  factory CityModel.fromJson(Map<String, dynamic> json) => CityModel(
    city: List<City>.from(json["city"].map((x) => City.fromJson(x))),
    success: json["success"],
    details: json["details"],
  );

  Map<String, dynamic> toJson() => {
    "city": List<dynamic>.from(city.map((x) => x.toJson())),
    "success": success,
    "details": details,
  };
}

class City {
  City({
    required this.id,
    required this.cityName,
  });

  int id;
  String cityName;

  factory City.fromJson(Map<String, dynamic> json) => City(
    id: json["id"],
    cityName: json["city_name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "city_name": cityName,
  };
}
