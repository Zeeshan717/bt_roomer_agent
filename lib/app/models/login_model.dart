// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'package:meta/meta.dart';
import 'dart:convert';

LoginModel loginModelFromJson(String str) => LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
    LoginModel({
        required this.id,
        required this.emailId,
        required this.fullName,
        required this.password,
        required this.mobile,
        required this.status,
        required this.imeiNo,
        required this.authKey,
        required this.success,
        required this.details,
    });

    int? id;
    String? emailId;
    String? fullName;
    String? password;
    String? mobile;
    String? status;
    String? imeiNo;
    String? authKey;
    String success;
    String details;

    factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        id: json["id"]!=null?json["id"]:0,
        emailId: json["email_id"],
        fullName: json["full_name"],
        password: json["password"],
        mobile: json["mobile"],
        status: json["status"],
        imeiNo: json["imei_no"],
        authKey: json["auth_key"],
        success: json["success"],
        details: json["details"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "email_id": emailId,
        "full_name": fullName,
        "password": password,
        "mobile": mobile,
        "status": status,
        "imei_no": imeiNo,
        "auth_key": authKey,
        "success": success,
        "details": details,
    };
}
