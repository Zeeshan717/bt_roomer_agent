
import 'dart:convert';

StateModel stateModelFromJson(String str) => StateModel.fromJson(json.decode(str));

String stateModelToJson(StateModel data) => json.encode(data.toJson());

class StateModel {
  StateModel({
    required this.state,
    required this.success,
    required this.details,
  });

  List<AllState> state;
  String success;
  String details;

  factory StateModel.fromJson(Map<String, dynamic> json) => StateModel(
    state: List<AllState>.from(json["state"].map((x) => AllState.fromJson(x))),
    success: json["success"],
    details: json["details"],
  );

  Map<String, dynamic> toJson() => {
    "state": List<dynamic>.from(state.map((x) => x.toJson())),
    "success": success,
    "details": details,
  };
}

class AllState {
  AllState({
    required this.id,
    required this.stateName,
  });

  int id;
  String stateName;

  factory AllState.fromJson(Map<String, dynamic> json) => AllState(
    id: json["id"],
    stateName: json["state_name"],
  );

  Map<String, dynamic> toJson() => {
    "id": id,
    "state_name": stateName,
  };
}
