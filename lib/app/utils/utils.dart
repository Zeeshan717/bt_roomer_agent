
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

pickImage(ImageSource source) async{

 XFile? _file = await ImagePicker().pickImage(source: source);

 if(_file != null){
   return _file.readAsBytes();
 }

 print("No file selected");

}

selectDate(BuildContext context) async {
 DateTime? now = await showDatePicker(
      context: context,
      initialDate: DateTime(DateTime.now().year - 1, 4, 1),
      firstDate: DateTime(DateTime.now().year - 1, 4, 1),
      lastDate: DateTime(DateTime.now().year, 3, 31));
  if(now !=null) {
    print("-------date---------"+now.toString());
    return now;
  }
  return null;
}
