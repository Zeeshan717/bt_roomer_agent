import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:bt_roomer_agent/app/models/agent_onboard_model.dart';
import 'package:bt_roomer_agent/app/models/city_model.dart';
import 'package:bt_roomer_agent/app/models/login_model.dart';
import 'package:http/http.dart' as http;

import '../models/agent_incentive_model.dart';
import '../models/state_model.dart';

class ApiService {
  static const Url = "https://dashboard.btroomer.com/api/";
  static Future<LoginModel> loginApi(
      String mobile,
      email_id,
      password,
      imei_no,
      ip_address,
      carrier_name,
      app_version,
      phone_model,
      phone_manufacturer,
      sdk_phone_version,
      device_id) async {
    var response = await http.post(Uri.parse(Url + "agentLogin"), body: {
      'mobile': mobile,
      'email_id': email_id,
      'password': password,
      'imei_no': imei_no,
      'ip_address': ip_address,
      'carrier_name': carrier_name,
      'app_version': app_version,
      'phone_model': phone_model,
      'phone_manufacturer': phone_manufacturer,
      'sdk_phone_version': sdk_phone_version,
      'device_id': device_id,
    });

    if (response.statusCode == 200) {
      print(response.body);
    }

    return loginModelFromJson(response.body);
  }

  static Future logoutApi(String agent_id) async {
    try {
      var response = await http.post(Uri.parse(Url + "agentLogin"), body: {
        'id': agent_id,
      });

      if (response.statusCode == 200) {
        print(response.body);
      }

      return jsonEncode(response.body);
    } catch (e) {
      print(e.toString());
    }
  }

  static Future onboardApi(
      String pg_name,
      agent_id,
      contact_1,
      contact_2,
      address,
      owner_name,
      owner_mail,
      stateId,
      cityId,
      branch_type,
      pan_number,
      pay_now,
      amount,
      paid_amount,
      payment_mode,
      payment_date,
      payment_status,
      transaction_id) async {
    var respons = await http.post(
        Uri.parse("https://dashboard.btroomer.com/api/agentOnboardHostel"),
        body: {
          'pg_name': pg_name,
          'agent_id': agent_id,
          'contact_1': contact_1,
          'contact_2': contact_2,
          'address': address,
          'owner_name': owner_name,
          'owner_mail': owner_mail,
          'stateId': stateId,
          'cityId': cityId,
          'branch_type': branch_type,
          'pan_number': pan_number,
          'pay_now': pay_now,
          'amount': amount,
          'paid_amount': paid_amount,
          'payment_mode': payment_mode,
          'payment_date': payment_date,
          'payment_status': payment_status,
          'transaction_id': transaction_id,
        });

    if (respons.statusCode == 200) {
      print(respons.body.toString());
      // jsonDecode(respons.body);
    }
    return jsonDecode(respons.body);
  }

  static Future<StateModel> getStateList() async {
    var response = await http
        .get(Uri.parse("https://dashboard.btroomer.com/api/agentStateList"));
    return stateModelFromJson(response.body);
  }

  static Future<CityModel> getCityList() async {
    var response = await http
        .get(Uri.parse("https://dashboard.btroomer.com/api/agentCityList"));
    return cityModelFromJson(response.body);
  }

  static Future<AgentOnboardModel> getAgentOnboardListApi(
      String agent_id) async {
    var response = await http.post(
        Uri.parse("https://dashboard.btroomer.com/api/agentOnboardList"),
        body: {'agent_id': agent_id});

    print(response.body);
    return agentOnboardModelFromJson(response.body);
  }

  static Future editClientDetailsApi(
      String pg_name, owner_name, address, pan_no, agent_id, pg_id) async {
    print(
        "${pg_name}, ${owner_name}, ${address}, ${pan_no}, ${agent_id}, ${pg_id}");
    var response = await http.post(
        Uri.parse("https://dashboard.btroomer.com/api/EditClientDetails"),
        body: {
          "pg_name": pg_name,
          "owner_name": owner_name,
          "address": address,
          "pan_no": pan_no.isEmpty || pan_no.isEmpty == null ? "N/A" : pan_no,
          "agent_id": agent_id,
          "pg_id": pg_id
        });

    if (response.statusCode == 200) {
      return jsonDecode(response.body);
    }
    return null;
  }

  static Future<AgentIncentiveDetailsModel> agentIncentiveDetailsApi(
      String agent_id) async {
    print("------------agent id-----------------${agent_id}");
    var response = await http.post(
        Uri.parse("https://dashboard.btroomer.com/api/agentIncentiveDetails"),
        body: {"agent_id": agent_id});
    print(response.body.toString());
    if (response.statusCode == 200) {
      print(response.body.toString());
    }
    return agentIncentiveDetailsModelFromJson(response.body);
  }

  static Future agentPaymentRequestApi(
    String agent_id,
    request_amount,
  ) async {
    var response = await http.post(
        Uri.parse("https://dashboard.btroomer.com/api/agentPaymentRequest"),
        body: {
          "agent_id": agent_id,
          "request_amount": request_amount,
        });
    return jsonDecode(response.body);
    ;
  }

  // Add Payment Api
  static Future addPaymentApi(
      String pg_id,
      agent_id,
      amount,
      payment_mode,
      payment_date,
      payment_status,
      transaction_id,
      File transaction_img) async {
    var postUri = Uri.parse("https://dashboard.btroomer.com/api/agentPayNow");
    var request = http.MultipartRequest("POST", postUri);
    request.fields['pg_id'] = pg_id;
    request.fields['agent_id'] = agent_id;
    request.fields['amount'] = amount;
    request.fields['payment_mode'] = payment_mode;
    request.fields['payment_date'] = payment_date;
    request.fields['payment_status'] = payment_status;
    request.fields['transaction_id'] = transaction_id;
    try {
      print("------Imag url-----------"+transaction_img.path.toString());
      request.files.add(await http.MultipartFile.fromPath(
          "transaction_img", transaction_img.path));
     // request.files.add(await http.MultipartFile.fromBytes("transaction_img", transaction_img));
    } catch (e) {
      print(e.toString());
      request.fields['transaction_img'] = "";
    }
    http.Response response =
        await http.Response.fromStream(await request.send());
    // print(response.body);
    return jsonDecode(response.body);
  }
}
