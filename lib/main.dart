import 'package:bt_roomer_agent/app/modules/incentives/incentives_view.dart';
import 'package:bt_roomer_agent/app/modules/login/login_view.dart';
import 'package:bt_roomer_agent/app/modules/onboard_clients/onboard_client_view.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

void main() async{
  await GetStorage.init();
  runApp( MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  GetStorage box = GetStorage();

  @override
  Widget build(BuildContext context) {
    bool isLoggedIn = box.read('isLoggedIn')==null?false:box.read('isLoggedIn');
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(fontFamily: 'Poppins'),
      home:isLoggedIn?OnboardClientView():LoginView(),
    );

  }
}